/**
  ******************************************************************************
  * @file    stm32_pereph_init.c
  * @brief   Main perepheral initialization routines
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"
#include "stm32_pereph_init.h"

uint16_t AdcOutArray[ADC_BUFFER];

void Peripherals_Hardware_Startup(void)
{
	Initialize_Clocks();
	Initialize_Pins();
	//Initialize_NVIC();
	Initialize_Timers();
	Initialize_USART();
 	Initialize_DMA();
	Initialize_ADC();
	Initialize_IWDG();

}

void Initialize_Clocks (void)
{
	/* RCC Init ----------------------------------------------------------- */
	RCC_DeInit();															// Resets the RCC clock configuration to the default reset state.
	RCC_HSEConfig(RCC_HSE_ON);												// Configures the External High Speed oscillator (HSE).
	while (RCC_WaitForHSEStartUp() == ERROR);								// Waits for HSE start-up.
	RCC_HCLKConfig(RCC_SYSCLK_Div1);										// Configures the AHB clock (HCLK).
	RCC_PCLK1Config(RCC_HCLK_Div2);											// Configures the Low Speed APB clock (PCLK1).
	RCC_PCLK2Config(RCC_HCLK_Div1);											// Configures the High Speed APB clock (PCLK2).
#ifdef STM32F10X_CL
	#define CORE_FREQ	72000000
	RCC_PREDIV2Config(RCC_PREDIV2_Div5);									// Configures the PREDIV2 division factor. (25Mhz/5=5Mhz)
	RCC_PLL2Config(RCC_PLL2Mul_8);											// Configures the PLL2 multiplication factor. (5Mhz*8=40Mhz)
	RCC_PLL2Cmd(ENABLE);													// Enables or disables the PLL2.
	RCC_PREDIV1Config(RCC_PREDIV1_Source_PLL2, RCC_PREDIV1_Div5);			// Configures the PREDIV1 division factor. (40Mhz/5=8Mhz)
	RCC_PLLConfig(RCC_PLLSource_PREDIV1, RCC_PLLMul_9);						// Configures the PLL clock source and multiplication factor. (8Mhz*9=72Mhz)
	RCC_ADCCLKConfig(RCC_PCLK2_Div6);										/* ADCCLK = PCLK2/2 = 24/2 = 12MHz*/ // Or 72/6 for 72MHz

	FLASH_PrefetchBufferCmd(FLASH_PrefetchBuffer_Enable);					// Enables or disables the Prefetch Buffer.
	FLASH_SetLatency(FLASH_Latency_2);										// Sets the code latency value.
#elif defined STM32F10X_MD_VL
	#define CORE_FREQ	24000000
	RCC_PLLConfig(RCC_PLLSource_PREDIV1, RCC_PLLMul_3);						// Configures the PLL clock source and multiplication factor. (9 for 72Mhz, 3 for 24Mhz)
	RCC_ADCCLKConfig(RCC_PCLK2_Div2);										/* ADCCLK = PCLK2/2 = 24/2 = 12MHz*/ // Or 72/6 for 72MHz
#else
	#define CORE_FREQ	72000000
	RCC_PLLConfig(RCC_PLLSource_HSE_Div1, RCC_PLLMul_9);					// Configures the PLL clock source and multiplication factor. (9 for 72Mhz, 3 for 24Mhz)
	RCC_ADCCLKConfig(RCC_PCLK2_Div6);										/* ADCCLK = PCLK2/2 = 24/2 = 12MHz*/ // Or 72/6 for 72MHz

	FLASH_PrefetchBufferCmd(FLASH_PrefetchBuffer_Enable);					// Enables or disables the Prefetch Buffer.
	FLASH_SetLatency(FLASH_Latency_2);										// Sets the code latency value.
#endif
	RCC_PLLCmd(ENABLE);														// Enables or disables the PLL.
	RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);								// Configures the system clock (SYSCLK).

}

void Initialize_Pins (void)
{
	GPIO_InitTypeDef GPIO_InitStructureCore_Hardware_Startup;
	GPIO_InitTypeDef GPIO_InitStructure;									// GPIO Conf Struct

	/* Configure all unused GPIO port pins in Analog Input mode (floating input
	 trigger OFF), this will reduce the power consumption and increase the device
	 immunity against EMI/EMC  */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB |
	RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOD, ENABLE);
	GPIO_InitStructureCore_Hardware_Startup.GPIO_Pin = GPIO_Pin_All;
	GPIO_InitStructureCore_Hardware_Startup.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_Init(GPIOA, &GPIO_InitStructureCore_Hardware_Startup);
	GPIO_Init(GPIOB, &GPIO_InitStructureCore_Hardware_Startup);
	GPIO_Init(GPIOC, &GPIO_InitStructureCore_Hardware_Startup);
	GPIO_Init(GPIOD, &GPIO_InitStructureCore_Hardware_Startup);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB |
	RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOD, DISABLE);

 	/* GPIO Init ---------------------------------------------------------- */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | 
	RCC_APB2Periph_GPIOC | RCC_APB2Periph_AFIO, ENABLE);					// Enables or disables the High Speed APB (APB2) peripheral clock.

		/* ADC pins */
	GPIO_StructInit(&GPIO_InitStructure);									// Fills each GPIO_InitStruct member with its default value.
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;								// Specifies the GPIO pins to be configured.
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;							// Specifies the operating mode for the selected pins.
	GPIO_Init(GPIOA, &GPIO_InitStructure);									// Initializes the GPIOx peripheral according to the specified parameters in the GPIO_InitStruct.

		/* LED pins */
	GPIO_StructInit(&GPIO_InitStructure);									// Fills each GPIO_InitStruct member with its default value.
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_3;					// Specifies the GPIO pins to be configured.
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 						// Specifies the speed for the selected pins.
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;						// Specifies the operating mode for the selected pins.
	GPIO_Init(GPIOC, &GPIO_InitStructure);									// Initializes the GPIOx peripheral according to the specified parameters in the GPIO_InitStruct.

		/* USART1 pins */
	GPIO_StructInit(&GPIO_InitStructure);									// Fills each GPIO_InitStruct member with its default value.
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;								// Specifies the GPIO pins to be configured.
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 						// Specifies the speed for the selected pins.
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;							// Specifies the operating mode for the selected pins.
	GPIO_Init(GPIOA, &GPIO_InitStructure);									// Initializes the GPIOx peripheral according to the specified parameters in the GPIO_InitStruct.

	GPIO_StructInit(&GPIO_InitStructure);									// Fills each GPIO_InitStruct member with its default value.
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;								// Specifies the GPIO pins to be configured.
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;					// Specifies the operating mode for the selected pins.
	GPIO_Init(GPIOA, &GPIO_InitStructure);									// Initializes the GPIOx peripheral according to the specified parameters in the GPIO_InitStruct.

		/* PWM pins */
	GPIO_StructInit(&GPIO_InitStructure);									// Fills each GPIO_InitStruct member with its default value.
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3;								// Specifies the GPIO pins to be configured.
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 						// Specifies the speed for the selected pins.
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;							// Specifies the operating mode for the selected pins.
	GPIO_Init(GPIOA, &GPIO_InitStructure);									// Initializes the GPIOx peripheral according to the specified parameters in the GPIO_InitStruct.

		/* Digital IN */
	GPIO_StructInit(&GPIO_InitStructure);									// Fills each GPIO_InitStruct member with its default value.
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_0;					// Specifies the GPIO pins to be configured.
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 						// Specifies the speed for the selected pins.
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;							// Specifies the operating mode for the selected pins.
	GPIO_Init(GPIOA, &GPIO_InitStructure);									// Initializes the GPIOx peripheral according to the specified parameters in the GPIO_InitStruct.
		
}

void Initialize_Timers (void)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;						// TIM Conf Struct
	TIM_OCInitTypeDef TIM_OCInitStructure;									// TIM OC Conf Struct

	/* TIM2 Configuration ------------------------------------------------ For Out PWM */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	TIM_DeInit(TIM2);
	TIM_TimeBaseStructInit(&TIM_TimeBaseInitStructure);
	TIM_TimeBaseInitStructure.TIM_Prescaler = CORE_FREQ/PWM_FREQ/0x10000 - 1;							// Specifies the prescaler value used to divide the TIM clock.
	TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;			// Specifies the counter mode.
	TIM_TimeBaseInitStructure.TIM_Period = 0x10000 - 1;						// 100 // Specifies the period value to be loaded into the active Auto-Reload Register at the next update event.
	TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;				// Specifies the clock division.
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseInitStructure);						// Initializes the TIMx Time Base Unit peripheral according to the specified parameters in the TIM_TimeBaseInitStruct.

		/* MON1 */
	TIM_OCStructInit(&TIM_OCInitStructure);									// Fills each TIM_OCInitStruct member with its default value.
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;						// Specifies the TIM mode.
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;			// Specifies the TIM Output Compare state.
	TIM_OCInitStructure.TIM_Pulse = 0x10000/2 - 1;								// Specifies the pulse value to be loaded into the Capture Compare Register.
	TIM_OC3Init(TIM2, &TIM_OCInitStructure);								// Initializes the TIMx Channel1 according to the specified parameters in the TIM_OCInitStruct.
	TIM_OC3PreloadConfig(TIM2, TIM_OCPreload_Enable);						// Enables or disables the TIMx peripheral Preload register on CCR1.

		/* MON2 */
	TIM_OCStructInit(&TIM_OCInitStructure);									// Fills each TIM_OCInitStruct member with its default value.
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;						// Specifies the TIM mode.
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;			// Specifies the TIM Output Compare state.
	TIM_OCInitStructure.TIM_Pulse = 0x10000/2 - 1;								// Specifies the pulse value to be loaded into the Capture Compare Register.
	TIM_OC4Init(TIM2, &TIM_OCInitStructure);								// Initializes the TIMx Channel1 according to the specified parameters in the TIM_OCInitStruct.
	TIM_OC4PreloadConfig(TIM2, TIM_OCPreload_Enable);						// Enables or disables the TIMx peripheral Preload register on CCR1.

	TIM_CtrlPWMOutputs(TIM2, ENABLE);										// Enables or disables the TIM peripheral Main Outputs.
	TIM_Cmd(TIM2, ENABLE);													// Enables or disables the specified TIM peripheral.

	/* TIM3 Configuration ------------------------------------------------- 1 ms timer */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	TIM_DeInit(TIM3);
	TIM_TimeBaseStructInit(&TIM_TimeBaseInitStructure);
	TIM_TimeBaseInitStructure.TIM_Prescaler = (uint16_t)(CORE_FREQ / 1000);	// Specifies the prescaler value used to divide the TIM clock.
	TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;			// Specifies the counter mode.
	TIM_TimeBaseInitStructure.TIM_Period = 10 - 1;							// Specifies the period value to be loaded into the active Auto-Reload Register at the next update event.
	TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;				// Specifies the clock division.
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseInitStructure);						// Initializes the TIMx Time Base Unit peripheral according to the specified parameters in the TIM_TimeBaseInitStruct.
// 	TIM_Cmd(TIM3, ENABLE);													// Enables or disables the specified TIM peripheral.

// 	TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);								// Enables or disables the specified TIM interrupts.
// 	NVIC_EnableIRQ(TIM3_IRQn);
	
}

void Initialize_ADC (void)
{
	ADC_InitTypeDef ADC_InitStructure;										// ADC Config Struct

	/* ADC1 Configuration -------------------------------------------------- */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);					/* Enable ADC1 clock so that we can talk to it */
	ADC_DeInit(ADC1);														// Deinitializes the ADCx peripheral registers to their default reset values.

	ADC_StructInit(&ADC_InitStructure);										// Fills each ADC_InitStruct member with its default value
	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;						// Simultanious mode on regular channels
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;							/* Disable the scan conversion so we do one at a time */
	ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;						/* Don't do continuous conversions - do them on demand */
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;		/* Start conversin by software, not an external trigger */
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;					/* Conversions are 12 bit - put them in the lower 12 bits of the result */
	ADC_InitStructure.ADC_NbrOfChannel = 1;									/* Say how many channels would be used by the sequencer */
	ADC_Init(ADC1, &ADC_InitStructure);										/* Now do the setup */
 	ADC_DMACmd(ADC1, ENABLE);												// Enables or disables the specified ADC DMA request.
	ADC_RegularChannelConfig(ADC1, ADC_Channel_1, 1, ADC_SampleTime_7Cycles5);	// Configures for the selected ADC regular channel its corresponding
	ADC_Cmd(ADC1, ENABLE);													/* Enable ADC1 */
	ADC_Cmd(ADC1, ENABLE);													/* Enable ADC1 */

	ADC_ResetCalibration(ADC1);												/* Enable ADC1 reset calibaration register */
	while(ADC_GetResetCalibrationStatus(ADC1));								/* Check the end of ADC1 reset calibration register */
	ADC_StartCalibration(ADC1);												/* Start ADC1 calibaration */
	while(ADC_GetCalibrationStatus(ADC1));									/* Check the end of ADC1 calibration */

}

void Initialize_DMA (void)
{
 	DMA_InitTypeDef DMA_InitStructure; 										// Variable used to setup the DMA
	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);						/* DMA1 clock enable */

	/* ADC to variable */
	DMA_DeInit(DMA1_Channel1); 												// Set DMA registers to default values
	DMA_StructInit(&DMA_InitStructure);										// Fills each DMA_InitStruct member with its default value.

	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &ADC1->DR; 		// Address of peripheral the DMA must map to
	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) &AdcOutArray[0]; 		// Variable to which ADC values will be stored
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;						// Specifies if the peripheral is the source or destination.
	DMA_InitStructure.DMA_BufferSize = ADC_BUFFER; 									// Specifies the buffer size, in data unit, of the specified Channel.
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;		// Specifies whether the Peripheral address register is incremented or not.
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;					// Specifies whether the memory address register is incremented or not.
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;	// Specifies the Peripheral data width.
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;			// Specifies the Memory data width.
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;							// Specifies the operation mode of the DMAy Channelx. (DMA_Mode_Normal | DMA_Mode_Circular)
	DMA_InitStructure.DMA_Priority = DMA_Priority_Low;						// Specifies the software priority for the DMAy Channelx.
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;							// Specifies if the DMAy Channelx will be used in memory-to-memory transfer.
	
	DMA_Init(DMA1_Channel1, &DMA_InitStructure); 							// Initialise the DMA
	DMA_Cmd(DMA1_Channel1, ENABLE); 										// Enable the DMA1 - Channel1
	
// 	/* SPI_RX DMA */
// 	DMA_DeInit(DMA1_Channel2); 												// Set DMA registers to default values
// 	DMA_StructInit(&DMA_InitStructure);										// Fills each DMA_InitStruct member with its default value.

// 	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)&Sensors[0]; 			// Address of array in memory
// 	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&SPI1->DR;			// Address of peripheral the DMA must map to
// 	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;						// Specifies if the peripheral is the source or destination.
// 	DMA_InitStructure.DMA_BufferSize = 5;				 					// Specifies the buffer size, in data unit, of the specified Channel.
// 	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;		// Specifies whether the Peripheral address register is incremented or not.
// 	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;					// Specifies whether the memory address register is incremented or not.
// 	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;	// Specifies the Peripheral data width.
// 	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;			// Specifies the Memory data width.
// 	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;							// Specifies the operation mode of the DMAy Channelx. (DMA_Mode_Normal | DMA_Mode_Circular)
// 	DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;					// Specifies the software priority for the DMAy Channelx.
// 	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;							// Specifies if the DMAy Channelx will be used in memory-to-memory transfer.
// 	
// 	DMA_Init(DMA1_Channel2, &DMA_InitStructure); 							// Initialize the DMA
// 	//DMA_Cmd(DMA1_Channel2, ENABLE); 										// Enable the DMA1 - Channel2

// 	/* SPI_TX DMA */
// 	DMA_DeInit(DMA1_Channel3); 												// Set DMA registers to default values
// 	DMA_StructInit(&DMA_InitStructure);										// Fills each DMA_InitStruct member with its default value.

// 	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)&TestDMASPI[0]; 			// Address of array in memory
// 	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&SPI1->DR;			// Address of peripheral the DMA must map to
// 	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;						// Specifies if the peripheral is the source or destination.
// 	DMA_InitStructure.DMA_BufferSize = 5;				 					// Specifies the buffer size, in data unit, of the specified Channel.
// 	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;		// Specifies whether the Peripheral address register is incremented or not.
// 	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;					// Specifies whether the memory address register is incremented or not.
// 	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;	// Specifies the Peripheral data width.
// 	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;			// Specifies the Memory data width.
// 	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;							// Specifies the operation mode of the DMAy Channelx. (DMA_Mode_Normal | DMA_Mode_Circular)
// 	DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;						// Specifies the software priority for the DMAy Channelx.
// 	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;							// Specifies if the DMAy Channelx will be used in memory-to-memory transfer.
// 	
// 	DMA_Init(DMA1_Channel3, &DMA_InitStructure); 							// Initialize the DMA
// 	//DMA_Cmd(DMA1_Channel3, ENABLE); 										// Enable the DMA1 - Channel3

// 	/* USART1_TX DMA */
// 	DMA_DeInit(DMA1_Channel4); 												// Set DMA registers to default values
// 	DMA_StructInit(&DMA_InitStructure);										// Fills each DMA_InitStruct member with its default value.

// 	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &USART1->DR;		// Address of peripheral the DMA must map to
//  	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) &SendBuff; 			// Variable to which ADC values will be stored
// 	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;						// Specifies if the peripheral is the source or destination.
// 	DMA_InitStructure.DMA_BufferSize = 10; 									// Specifies the buffer size, in data unit, of the specified Channel.
// 	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;		// Specifies whether the Peripheral address register is incremented or not.
// 	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;					// Specifies whether the memory address register is incremented or not.
// 	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;	// Specifies the Peripheral data width.
// 	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;			// Specifies the Memory data width.
// 	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;							// Specifies the operation mode of the DMAy Channelx.
// 	DMA_InitStructure.DMA_Priority = DMA_Priority_Medium;						// Specifies the software priority for the DMAy Channelx.
// 	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;							// Specifies if the DMAy Channelx will be used in memory-to-memory transfer.
// 	
// 	DMA_Init(DMA1_Channel4, &DMA_InitStructure); 							// Initialize the DMA
// 	//DMA_Cmd(DMA1_Channel4, ENABLE); 										// Enable the DMA1 - Channel4

// 	/* USART2_TX DMA */
// 	DMA_DeInit(DMA1_Channel7); 												// Set DMA registers to default values
// 	DMA_StructInit(&DMA_InitStructure);										// Fills each DMA_InitStruct member with its default value.

// 	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &USART2->DR;		// Address of peripheral the DMA must map to
//  	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) &SendBuff2; 			// Variable to which ADC values will be stored
// 	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;						// Specifies if the peripheral is the source or destination.
// 	DMA_InitStructure.DMA_BufferSize = 3; 									// Specifies the buffer size, in data unit, of the specified Channel.
// 	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;		// Specifies whether the Peripheral address register is incremented or not.
// 	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;					// Specifies whether the memory address register is incremented or not.
// 	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;	// Specifies the Peripheral data width.
// 	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;			// Specifies the Memory data width.
// 	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;							// Specifies the operation mode of the DMAy Channelx.
// 	DMA_InitStructure.DMA_Priority = DMA_Priority_Medium;						// Specifies the software priority for the DMAy Channelx.
// 	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;							// Specifies if the DMAy Channelx will be used in memory-to-memory transfer.
// 	
// 	DMA_Init(DMA1_Channel7, &DMA_InitStructure); 							// Initialize the DMA
// 	DMA_Cmd(DMA1_Channel7, ENABLE); 										// Enable the DMA1 - Channel7

}

void Initialize_USART (void)
{
	USART_InitTypeDef USART_InitStructure;									// USART1 Conf Struct

	/* USART1 Configuration ------------------------------------------------ */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);					// Enables or disables the High Speed APB (APB2) peripheral clock.
	USART_DeInit(USART1);													// Deinitializes the USARTx peripheral registers to their default reset values.
	USART_StructInit(&USART_InitStructure);									// Fills each USART_InitStruct member with its default value.
	USART_InitStructure.USART_BaudRate = 460800;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_Init(USART1, &USART_InitStructure);								// Initializes the USARTx peripheral according to the specified parameters in the USART_InitStruct.
	USART_DMACmd(USART1, USART_DMAReq_Tx, ENABLE);							// Enables or disables the USARTs DMA interface.
	USART_Cmd(USART1, ENABLE);												// Enables or disables the specified USART peripheral.

// 	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);							// Enables or disables the specified USART interrupts. (Receive Data register not empty interrupt)
// 	NVIC_EnableIRQ(USART1_IRQn);
	
}

void Initialize_IWDG (void)
{
	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);							// Enables or disables write access to IWDG_PR and IWDG_RLR registers.
	IWDG_SetPrescaler(IWDG_Prescaler_4);									// Sets IWDG Prescaler value. (4-256)
	IWDG_SetReload(200);													// Тактируется от LSI 40кГц. (40000 / 4 / 200 = 20мс)
																			// Sets IWDG Reload value. This parameter must be a number between 0 and 0x0FFF.
	IWDG_Enable();															// Enables IWDG (write access to IWDG_PR and IWDG_RLR registers disabled).
}


/* EOF */
