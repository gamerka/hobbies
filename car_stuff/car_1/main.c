// http://compcar.ru  

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"
#include <stdio.h>
// #include <string.h>
#include <math.h>
#include "stm32_pereph_init.h"
#include "main.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define AV_PIN		GPIO_Pin_3
#define LED_PIN		GPIO_Pin_8
#define REV_PIN		GPIO_Pin_4
// #define USER_PIN	GPIO_Pin_0

#define CAM_ON_WAIT		500
#define CAM_OFF_WAIT	5000
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
extern uint16_t AdcOutArray[ADC_BUFFER];
uint32_t Light = 0;
uint32_t Brightness[2] = {1,1};
uint32_t BrightnessPrev[2] = {1,1};
uint32_t BrightnessVsLight1[2][8] = {
		{0, 0x640, 0xB00,  0xD00,  0xDD0,  0xFFF},
		{1, 0xA00, 0x1700, 0x3000, 0x5000, 0xFFFF}
		};
uint32_t BrightnessVsLight2[2][8] = {
		{0, 0x640, 0xB00,  0xD00,  0xDD0,  0xFFF},
		{1, 0xA00, 0x1700, 0x3000, 0x5000, 0xFFFF}
		};
uint8_t K = 1;																	// Коэффициент усиления по скорости нарастания яркости
uint32_t SysClk;
uint16_t ReverseOnCnt;
uint16_t ReverseOffCnt;
uint16_t ButtonDownCnt;
uint8_t  PWM_Prescaler = 0;


/* Private function prototypes -----------------------------------------------*/
void WaitMs (uint16_t Ms);
uint32_t TableLookup1d(uint32_t Inp, uint32_t In[][8], uint32_t ArrSize);
/* Private functions ---------------------------------------------------------*/
int main (void)
{
// 	for (int i = 0; i < 8; i++)
// 	{
// 		BrightnessVsLight[0][i] <<= 4;
// 	}
// 	for (int i = 0; i < 8; i++)
// 	{
// 		BrightnessVsLight[1][i] <<= 8;
// 	}
	
	Peripherals_Hardware_Startup();
	
	while (1)
	{
		uint32_t LightTemp = 0;
		SysClk++;
		
	/* Автоматическая яркость */
		for (int i = 0; i < ADC_BUFFER; i++)
		{
			LightTemp += (uint32_t)AdcOutArray[i];
		}
		Light = LightTemp >> 8;														// Текущее значение освещенности
		Brightness[0] = TableLookup1d(Light, BrightnessVsLight1, 6);					// Желаемое значение яркости
		Brightness[1] = TableLookup1d(Light, BrightnessVsLight2, 6);					// Желаемое значение яркости
		
		for (char Mon = 0; Mon < 2; Mon++)
		{
			if (Brightness[Mon] > BrightnessPrev[Mon])											// Делаем ярче
			{
				Brightness[Mon] = BrightnessPrev[Mon] + K * (Brightness[Mon] - BrightnessPrev[Mon]) / 1000 + 1;
			}
			else if (Brightness[Mon] < BrightnessPrev[Mon])										// Делаем темнее
			{
				Brightness[Mon] = BrightnessPrev[Mon] - K * (BrightnessPrev[Mon] - Brightness[Mon]) / 1000 - 1;
			}
			BrightnessPrev[Mon] = Brightness[Mon];												// Текущее значение яркости
		}
	
		TIM_SetCompare3(TIM2, Brightness[0]);	
		TIM_SetCompare4(TIM2, Brightness[1]);	
		
	/* Задержка камеры заднего хода */
		if (GPIO_ReadInputDataBit(GPIOA, REV_PIN))									// Задняя передача включена
		{
			ReverseOffCnt = 0;
			if (!GPIO_ReadOutputDataBit(GPIOC, AV_PIN))								// Камера не включена
			{
				ReverseOnCnt++;
				if (ReverseOnCnt > CAM_ON_WAIT)										// Посчитали 500 мс
				{
					GPIO_SetBits(GPIOC, AV_PIN);									// Включили камеру
					GPIO_SetBits(GPIOC, LED_PIN);									
					ReverseOnCnt = 0;
				}
			}
		}
		else																		// Задняя передача выключена
		{
			ReverseOnCnt = 0;
			if (GPIO_ReadOutputDataBit(GPIOC, AV_PIN))								// Камера включена
			{
				ReverseOffCnt++;
				if (ReverseOffCnt > CAM_OFF_WAIT)									// Посчитали 5000 мс
				{
					GPIO_ResetBits(GPIOC, AV_PIN);									// Включили камеру
					GPIO_ResetBits(GPIOC, LED_PIN);
					ReverseOffCnt = 0;
				}
			}
		}

	/* Обработка кнопки USER1 */
// 		if (GPIO_ReadInputDataBit(GPIOA, USER_PIN))									// Кнопка нажата
// 		{
// 			ButtonDownCnt++;														// Ожидание
// 			if (ButtonDownCnt > 500)												// Дождались
// 			{
// 				PWM_Prescaler++;
// 				if (PWM_Prescaler > 3) PWM_Prescaler = 0;
// 				TIM_PrescalerConfig(TIM2, PWM_Prescaler, TIM_PSCReloadMode_Update);
// 				ButtonDownCnt = 0;
// 			}
// 				
// 				
// 		}
// 		else
// 		{
// 			ButtonDownCnt = 0;
// 		}
// 		
		

		
		WaitMs(1);
		
		GPIO_WriteBit(GPIOC, GPIO_Pin_9, (BitAction)(!GPIO_ReadOutputDataBit(GPIOC, GPIO_Pin_9)));
		IWDG_ReloadCounter();
	}
	
	
	
	
	
	
	
}

void WaitMs (uint16_t Ms)
{
	TIM_SetCounter(TIM3, 0);
	if (Ms == 1)
	{
		TIM_PrescalerConfig(TIM3, (uint16_t)(24000000 / 10000), TIM_PSCReloadMode_Update);
		Ms = 10;
	}
	else
	{
		TIM_PrescalerConfig(TIM3, (uint16_t)(24000000 / 1000), TIM_PSCReloadMode_Update);
	}
		
	TIM_SetAutoreload(TIM3, Ms - 1);
	TIM_Cmd(TIM3, ENABLE);													// Enables or disables the specified TIM peripheral.
	while (!(TIM_GetFlagStatus(TIM3, TIM_FLAG_Update)));
	TIM_ClearFlag(TIM3, TIM_FLAG_Update);
	TIM_Cmd(TIM3, DISABLE);													// Enables or disables the specified TIM peripheral.
	
}

/**
  * @brief  Returns 1-d table lookup result
  * @param  Inp: lookuped value
  * @param  In: Array 
  * @param  ArrSize: Array 2-nd dimension size
  * @retval Result value
  */
uint32_t TableLookup1d(uint32_t Inp, uint32_t In[][8], uint32_t ArrSize)
{
	uint32_t Outp = 0;
	int i = 0;
	
	/* Обрезка по краям */
	if (Inp >= In[0][ArrSize-1]) Outp = In[1][ArrSize-1];	
	else if (Inp <= In[0][0]) Outp = In[1][0];
	/* */
	else 
	{
		for (i = 1; i < ArrSize; i++)
		{
			/* Поиск верхней границы */
			if (Inp <= In[0][i])
			{
				/* val_x = val[n-1] + (val[n] - val[n-1]) * (ind_x - ind[n-1]) / (ind[n] - ind[n-1]) */
				Outp = In[1][i-1] + (In[1][i] - In[1][i-1]) * (Inp - In[0][i-1]) / (In[0][i] - In[0][i-1]);
				break;
			}
		}
	}
	
	return Outp;
}






// // циклы опроса процедур
// unsigned long mills_poll_REVL=0,
//               mills_poll_AUTO_LIGHT=0,
//               mills_poll_AMP=AMP_DELAY,
//               mills_poll_SAVG=SAVG_DELAY;

// // флаги
//  
// void setup(){
// 	// вкл usart				Serial.begin(115200);  
// 	// чтение состояния заджней лампы	revl_state=EEPROM.read(REVL_STATE_OFFSET);
//   // чтение датчика освещенности	sens = analogRead(LIGHT_SENS)>>2; // установили первичное значение усредненного датчика освещенности

// }  

// void loop(){
//   // если текущие миллисекунды больше интервала поллинга
// POLL(SAVG);
// POLL(AUTO_LIGHT);
// //POLL(AMP);
// POLL(REVL);
// }


// void poll_SAVG(){ // среднее значение датчика освещенности

// int sens_ = analogRead(LIGHT_SENS)>>2; //прочитали текущеее показание датчика

//  if (sens_ > sens) {  // если текущее больше среднего - двигаем среднее вверх на одну позицию
//     sens = sens + 1;
//   } 

//  if (sens_ < sens) {  // если текущее меньше среднего - двигаем среднее вниз на одну позицию
//     sens = sens - 1;
//   } 

// //Serial.println(">>>*****");               
// Serial.print(" sens_=");
// Serial.print(sens_, DEC);
// //Serial.print(" sens=");
// Serial.print(" / ");
// Serial.println(sens, DEC);               
// Serial.println("*****");               

//   p_delay(SAVG,SAVG_DELAY); // задержка для следующего опроса.

// }

// //--------------------------------
// // Опрос автоматической корретировки света
// // алгоритм работы взят у ув. CHIP-а (all-in-one project)
// // изменения: сравнения с кратностью 30 заменены на сдвиг вправо на 5 битов (деление на 32)
// //            введен сдвиговый коэфициент для корректировки яркости с пульта
// //            свет=(свет>>4 + коэф_свет - МАКС_СВЕТ_КОЭФФ/2)
// // для простоты показываю изменение яркости в винде от датчика и кнопок на пульте
// // думаю, что будет удобно. если не настраивать кнопок работать будет по алгоритму CHIP
// // \датчик| 0       32      64      128     160     192     224
// // коэф \ |-----------------------------------------------------
// // 0      | 2       2       2       64      96      128     160
// // 1      | 2       2       64      96      128     160     192
// // 2      | 2       2       64      96      128     160     192
// // 3      | 2       64      96      128     160     192     255
// // 4      | 2       64      96      128     160     192     255
// // 5      | 64      96      128     160     192     255     255
// // 6      | 64      96      128     160     192     255     255

// void poll_AUTO_LIGHT(){ // управление яркостью
// int light;   
// byte light_grandis;   
// // byte last_light;
// // int AUTO_LIGHT_DELAY = 3000;
//  // int sens = analogRead(LIGHT_SENS)>>2;   // /4 :) // интенсивность
// //int sens = analogRead(LIGHT_SENS);

// // sens=((sens>>4)+ulk-MAX_ULK/2)>>1;  // обработка коэффициента
// // light=sens<=0?2:(sens>=6?255:(sens+1)<<5);


// //int sens_grandis = analogRead(LIGHT_SENS_GRANDIS)>>2;
// // int light_sw = analogRead(LIGHT_SWITCH)>>2;

// //Serial.print(" sens=");
// //Serial.println(sens, DEC);
// //Serial.print(" sens_sred=");
// //Serial.print(sens_sred, DEC);

// //Serial.print(" insred=");
// //Serial.print(InSrednee, DEC);


// //Serial.print(" sensgrandis=");
// //Serial.print(sens_grandis, DEC);

//  
// // sens=((sens>>4)+ulk-MAX_ULK/2)>>1;  // обработка коэффициента
// // light=sens<=0?2:(sens>=6?255:(sens+1)<<5);


// //регулировка по датчику освещенности------------
// //if ((sens > 0) && (sens<=917)) {light = 1;}
// //if ((sens > 917) && (sens<=918)) {light = 5;}
// //if ((sens > 918) && (sens<=920)) {light = 10;}
// //if ((sens > 920) && (sens<=970)) {light = 15;}
// //if ((sens > 970) && (sens<=975)) {light = 80;}
// //if ((sens > 975) && (sens<=980)) {light = 100;}
// //if ((sens > 980) && (sens<=990)) {light = 130;}
// //if ((sens > 990) && (sens<=1000)) {light = 160;}
// //if ((sens > 1000) && (sens<=1010)) {light = 200;}
// //if ((sens > 1010) && (sens<=1020)) {light = 230;}
// //if (sens > 1020) {light = 255;}
// //-------------------
// if ((sens > 0) && (sens<=5)) {light = 1;}   
// if ((sens > 5) && (sens<=10)) {light = 2;}   
// if ((sens > 10) && (sens<=15)) {light = 3;}
// if ((sens > 15) && (sens<=20)) {light = 4;}
// if ((sens > 20) && (sens<=30)) {light = 5;}
// if ((sens > 30) && (sens<=40)) {light = 6;}
// if ((sens > 40) && (sens<=50)) {light = 7;}
// if ((sens > 50) && (sens<=60)) {light = 8;}
// if ((sens > 60) && (sens<=80)) {light = 9;}
// if ((sens > 80) && (sens<=100)) {light = 10;}
// if ((sens > 100) && (sens<=120)) {light = 15;}
// if ((sens > 120) && (sens<=140)) {light = 20;}
// if ((sens > 140) && (sens<=170)) {light = 30;}   
// if ((sens > 170) && (sens<=190)) {light = 40;}   
// if ((sens > 190) && (sens<=200)) {light = 50;}
// if ((sens > 200) && (sens<=210)) {light = 70;}
// if ((sens > 210) && (sens<=215)) {light = 90;}
// if ((sens > 215) && (sens<=220)) {light = 100;}
// if ((sens > 220) && (sens<=225)) {light = 200;}
// //if ((sens > 225) && (sens<=230)) {light = 150;}
// //if ((sens > 230) && (sens<=232)) {light = 200;}

// if (sens > 225)  {light = 255;}   



// // регулировка из проводки автомобиля
// //if ((sens_grandis > 10) && (sens_grandis<100)) {light_grandis = 255;}
// //if ((sens_grandis > 100) && (sens_grandis<150)) {light_grandis = 150;}
// //if ((sens_grandis > 150) && (sens_grandis<200)) {light_grandis = 50;}
// //if ((sens_grandis > 200) && (sens_grandis<254)) {light_grandis = 10;}
// //-----------------------

// // проверяем включена ли перемычка
// //  if (analogRead(LIGHT_SWITCH) < 1000) {
// //    light = light_grandis;
// //    AUTO_LIGHT_DELAY = 1000;
// //  }

// //Serial.print(" sw=");
// //Serial.print(analogRead(LIGHT_SWITCH), DEC);

// //Serial.print(" del=");
// //Serial.print(AUTO_LIGHT_DELAY, DEC);
//  
// //Serial.print(" light=");
// //Serial.print(light, DEC);


//  //if (light!=last_light) {
//  //   analogWrite(LIGHT_PIN, light);
//  //   last_light=light;
//  // }
// //int fadeValue;
// //fadeValue = last_light;

// //Serial.print(" last_light=");
// //Serial.println(last_light, DEC);

// Serial.print(" light=");
// Serial.println(light, DEC);

// //light = light * 100;
// //last_light = last_light * 100;

//  if (light >  last_light) {
//  
//  // fade in from min to max in increments of 1 points:

//   for(int fadeValue = last_light; fadeValue <= light; fadeValue +=1) { 
//     // sets the value (range from 0 to 255):
// //    fadeValue=fadeValue / 100;
//     analogWrite(LIGHT_PIN, fadeValue);         
//     // wait for 30 milliseconds to see the dimming effect    
//     delay(30);                            
//   } 
//  }
//  
//  if (light < last_light) {
//  
//  // fade in from max to min in increments of 1 points:
//   for(int fadeValue = last_light; fadeValue >= light; fadeValue -=1) { 
//     // sets the value (range from 0 to 255):
//     analogWrite(LIGHT_PIN, fadeValue);         
//     // wait for 30 milliseconds to see the dimming effect    
//     delay(30);                            
//   } 
//  }
//   
// last_light=light;
// //Serial.print(" last_light=");
// //Serial.print(last_light, DEC);
//  
//   p_delay(AUTO_LIGHT,AUTO_LIGHT_DELAY); // задержка для следующего опроса.
// }

// //--------------------------------
// // обработка переключения моника на камеру заднего вида
// // задача не требуется обработки в реальном времени, 
// // поэтому упрощаем алгоритм до срабатывания 1 раз в REVL_DELAY мс
// void poll_REVL(){ 
// byte temp;
// temp=revl_state;
//   if (digitalRead(REVL_PIN) != HIGH){
//     switch (revl_state) {
//       case 0:
//         revl_state=1;
//         break;
//       case 1:
//         revl_state=2;
//         digitalWrite(AVSW_PIN, HIGH);  // подать сигнал на включение камеры  
//     }
//   } else {
//     if (revl_state==2) {
//       delay(REVL_DELAY_OFF);
//       digitalWrite(AVSW_PIN, LOW);  // снять сигнал 
//     }
//    
//     revl_state=0;
//    
//   }
//    p_delay(REVL,REVL_DELAY); // задержка для следующего опроса.

// if (temp!=revl_state){
//     EEPROM.write(REVL_STATE_OFFSET,revl_state);
// }
// }


