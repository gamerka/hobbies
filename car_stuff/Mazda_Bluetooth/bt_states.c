#include "bt_states.h"

uint32_t sysClock = 0;

BtState_TypeDef CurrentBtState;

uint8_t BlueState;
uint32_t blueOnTime;
uint32_t blueOffTime;
uint32_t BlueOnPeriodLast;

uint8_t RedState;
uint32_t redOnTime;
uint32_t redOffTime;
uint32_t RedOnPeriodLast;

uint32_t btPowerOnPressTime;

void UpdateBtState(void)
{
	// Если оба горят
	if (RedState && BlueState)
	{
		// если горят хоть немножко - в любом случае признак включения
		if (RedTimeSinceOn() > LED_BLINK_MIN_PERIOD && BlueTimeSinceOn() > LED_BLINK_MIN_PERIOD)
		{
			CurrentBtState = Bt_State_On;
		}
	}
	// если оба не горят
	else if (!RedState && !BlueState)
	{
		// и это продолжается уже дольше максимального периода мигания
		if (RedTimeSinceOff() > LED_BLINK_MAX_PERIOD && BlueTimeSinceOff() > LED_BLINK_MAX_PERIOD)
		{
			CurrentBtState = Bt_State_Off;
		}
	}
	// если горит только красный
	else if (RedState && !BlueState)
	{
		// при этом недавно горел синий, а до этого снова красный
		if (BlueTimeSinceOff() < LED_BLINK_SEARCH_PERIOD && RedTimeSinceOff() < LED_BLINK_SEARCH_PERIOD * 2)
		{
			CurrentBtState = Bt_State_Searching;
		}
		// горит дольше чем при поиске
		else if (RedTimeSinceOn() > LED_BLINK_SEARCH_PERIOD + LED_BLINK_MIN_PERIOD)
		{
			CurrentBtState = Bt_State_SwitchingOff;
		}
	}
	// если горит только синий
	else if (!RedState && BlueState)
	{
		// если прошло еще слишком мало времени после включения
		if (sysClock - btPowerOnPressTime < MIN_POWER_ON_BOOT_TIME)
		{
			return;
		}
		// если дольше чем при стендбае, а до этого горел примерно как при проигрывании
		else if (BlueTimeSinceOn() > LED_BLINK_STANDBY_PERIOD + LED_BLINK_MIN_PERIOD &&
		CheckBlinkPeriodRough(BlueOnPeriodLast, LED_BLINK_PLAYING_PERIOD))
		{
			CurrentBtState = Bt_State_Playing;
		}
		// если еще не долго, но до этого как при стендбае
		else if (BlueTimeSinceOn() > LED_BLINK_MIN_PERIOD && 
			CheckBlinkPeriodRough(BlueOnPeriodLast, LED_BLINK_STANDBY_PERIOD))
		{
			CurrentBtState = Bt_State_StandBy;
		}
	}
}


inline uint8_t CheckBlinkPeriodRough(uint32_t checkedValue, uint32_t expectedValue)
{
	if (checkedValue < expectedValue + LED_BLINK_MIN_PERIOD && checkedValue > expectedValue - LED_BLINK_MIN_PERIOD) return 1;
	else return 0;
}


inline uint32_t BlueTimeSinceOn(void)
{
	int t = (int)(sysClock - blueOnTime);
	if (t < 0) t = 0;
	return t;
}
inline uint32_t RedTimeSinceOn(void)
{
	int t = (int)(sysClock - redOnTime);
	if (t < 0) t = 0;
	return t;
}

inline uint32_t BlueTimeSinceOff(void)
{
	int t = (int)(sysClock - blueOffTime);
	if (t < 0) t = 0;
	return t;
}
inline uint32_t RedTimeSinceOff(void)
{
	int t = (int)(sysClock - redOffTime);
	if (t < 0) t = 0;
	return t;
}

