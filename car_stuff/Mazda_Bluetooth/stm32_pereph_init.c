/**
  ******************************************************************************
  * @file    stm32_pereph_init.c
  * @brief   Main perepheral initialization routines
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"
#include "stm32_pereph_init.h"

void Peripherals_Hardware_Startup(void)
{
	Initialize_Clocks();
	Initialize_Pins();
	//Initialize_NVIC();
	Initialize_Timers();
	//Initialize_USART();
 	//Initialize_DMA();
	//Initialize_ADC();
	//Initialize_IWDG();
	Initialize_EXTI();
}

void Initialize_Clocks (void)
{
	/* RCC Init ----------------------------------------------------------- */
	RCC_DeInit();															// Resets the RCC clock configuration to the default reset state.
	RCC_HSEConfig(RCC_HSE_ON);												// Configures the External High Speed oscillator (HSE).
	while (RCC_WaitForHSEStartUp() == ERROR);								// Waits for HSE start-up.
	RCC_HCLKConfig(RCC_SYSCLK_Div1);										// Configures the AHB clock (HCLK).
	RCC_PCLK1Config(RCC_HCLK_Div2);											// Configures the Low Speed APB clock (PCLK1).
	RCC_PCLK2Config(RCC_HCLK_Div1);											// Configures the High Speed APB clock (PCLK2).
#ifdef STM32F10X_CL
	#define CORE_FREQ	72000000
	RCC_PREDIV2Config(RCC_PREDIV2_Div5);									// Configures the PREDIV2 division factor. (25Mhz/5=5Mhz)
	RCC_PLL2Config(RCC_PLL2Mul_8);											// Configures the PLL2 multiplication factor. (5Mhz*8=40Mhz)
	RCC_PLL2Cmd(ENABLE);													// Enables or disables the PLL2.
	RCC_PREDIV1Config(RCC_PREDIV1_Source_PLL2, RCC_PREDIV1_Div5);			// Configures the PREDIV1 division factor. (40Mhz/5=8Mhz)
	RCC_PLLConfig(RCC_PLLSource_PREDIV1, RCC_PLLMul_9);						// Configures the PLL clock source and multiplication factor. (8Mhz*9=72Mhz)
	RCC_ADCCLKConfig(RCC_PCLK2_Div6);										/* ADCCLK = PCLK2/2 = 24/2 = 12MHz*/ // Or 72/6 for 72MHz

	FLASH_PrefetchBufferCmd(FLASH_PrefetchBuffer_Enable);					// Enables or disables the Prefetch Buffer.
	FLASH_SetLatency(FLASH_Latency_2);										// Sets the code latency value.
#elif defined STM32F10X_MD_VL
	#define CORE_FREQ	24000000
	RCC_PLLConfig(RCC_PLLSource_PREDIV1, RCC_PLLMul_3);						// Configures the PLL clock source and multiplication factor. (9 for 72Mhz, 3 for 24Mhz)
	RCC_ADCCLKConfig(RCC_PCLK2_Div2);										/* ADCCLK = PCLK2/2 = 24/2 = 12MHz*/ // Or 72/6 for 72MHz
#else
	#define CORE_FREQ	72000000
	RCC_PLLConfig(RCC_PLLSource_HSE_Div1, RCC_PLLMul_9);					// Configures the PLL clock source and multiplication factor. (9 for 72Mhz, 3 for 24Mhz)
	RCC_ADCCLKConfig(RCC_PCLK2_Div6);										/* ADCCLK = PCLK2/2 = 24/2 = 12MHz*/ // Or 72/6 for 72MHz

	FLASH_PrefetchBufferCmd(FLASH_PrefetchBuffer_Enable);					// Enables or disables the Prefetch Buffer.
	FLASH_SetLatency(FLASH_Latency_2);										// Sets the code latency value.
#endif
	RCC_PLLCmd(ENABLE);														// Enables or disables the PLL.
	RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);								// Configures the system clock (SYSCLK).

}

void Initialize_Pins (void)
{
	GPIO_InitTypeDef GPIO_InitStructureCore_Hardware_Startup;
	GPIO_InitTypeDef GPIO_InitStructure;									// GPIO Conf Struct

	/* Configure all unused GPIO port pins in Analog Input mode (floating input
	 trigger OFF), this will reduce the power consumption and increase the device
	 immunity against EMI/EMC  */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB |
	RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOD, ENABLE);
	GPIO_InitStructureCore_Hardware_Startup.GPIO_Pin = GPIO_Pin_All;
	GPIO_InitStructureCore_Hardware_Startup.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_Init(GPIOA, &GPIO_InitStructureCore_Hardware_Startup);
	GPIO_Init(GPIOB, &GPIO_InitStructureCore_Hardware_Startup);
	GPIO_Init(GPIOC, &GPIO_InitStructureCore_Hardware_Startup);
	GPIO_Init(GPIOD, &GPIO_InitStructureCore_Hardware_Startup);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB |
	RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOD, DISABLE);

 	/* GPIO Init ---------------------------------------------------------- */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | 
	RCC_APB2Periph_GPIOC | RCC_APB2Periph_AFIO, ENABLE);					// Enables or disables the High Speed APB (APB2) peripheral clock.

		/* ADC pins */
//	GPIO_StructInit(&GPIO_InitStructure);									// Fills each GPIO_InitStruct member with its default value.
//	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;								// Specifies the GPIO pins to be configured.
//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;							// Specifies the operating mode for the selected pins.
//	GPIO_Init(GPIOA, &GPIO_InitStructure);									// Initializes the GPIOx peripheral according to the specified parameters in the GPIO_InitStruct.

		/* LED pins */
	GPIO_StructInit(&GPIO_InitStructure);									// Fills each GPIO_InitStruct member with its default value.
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9;					// Specifies the GPIO pins to be configured.
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 						// Specifies the speed for the selected pins.
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;						// Specifies the operating mode for the selected pins.
	GPIO_Init(GPIOC, &GPIO_InitStructure);									// Initializes the GPIOx peripheral according to the specified parameters in the GPIO_InitStruct.

		/* USART1 pins */
	GPIO_StructInit(&GPIO_InitStructure);									// Fills each GPIO_InitStruct member with its default value.
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;								// Specifies the GPIO pins to be configured.
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 						// Specifies the speed for the selected pins.
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;							// Specifies the operating mode for the selected pins.
	GPIO_Init(GPIOA, &GPIO_InitStructure);									// Initializes the GPIOx peripheral according to the specified parameters in the GPIO_InitStruct.

	GPIO_StructInit(&GPIO_InitStructure);									// Fills each GPIO_InitStruct member with its default value.
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;								// Specifies the GPIO pins to be configured.
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;					// Specifies the operating mode for the selected pins.
	GPIO_Init(GPIOA, &GPIO_InitStructure);									// Initializes the GPIOx peripheral according to the specified parameters in the GPIO_InitStruct.

		/* PWM pins */
//	GPIO_StructInit(&GPIO_InitStructure);									// Fills each GPIO_InitStruct member with its default value.
//	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3;								// Specifies the GPIO pins to be configured.
//	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 						// Specifies the speed for the selected pins.
//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;							// Specifies the operating mode for the selected pins.
//	GPIO_Init(GPIOA, &GPIO_InitStructure);									// Initializes the GPIOx peripheral according to the specified parameters in the GPIO_InitStruct.

		/* Digital IN */
	GPIO_StructInit(&GPIO_InitStructure);									// Fills each GPIO_InitStruct member with its default value.
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_0;					// Specifies the GPIO pins to be configured.
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 						// Specifies the speed for the selected pins.
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;							// Specifies the operating mode for the selected pins.
	GPIO_Init(GPIOA, &GPIO_InitStructure);									// Initializes the GPIOx peripheral according to the specified parameters in the GPIO_InitStruct.
		
		/* Digital OUT */
	GPIO_StructInit(&GPIO_InitStructure);									// Fills each GPIO_InitStruct member with its default value.
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;					// Specifies the GPIO pins to be configured.
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 						// Specifies the speed for the selected pins.
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;						// Specifies the operating mode for the selected pins.
	GPIO_Init(GPIOA, &GPIO_InitStructure);									// Initializes the GPIOx peripheral according to the specified parameters in the GPIO_InitStruct.

}

void Initialize_Timers (void)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;						// TIM Conf Struct
//	TIM_OCInitTypeDef TIM_OCInitStructure;									// TIM OC Conf Struct

	/* TIM2 Configuration ------------------------------------------------ For Out PWM */
//	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
//	TIM_DeInit(TIM2);
//	TIM_TimeBaseStructInit(&TIM_TimeBaseInitStructure);
//	TIM_TimeBaseInitStructure.TIM_Prescaler = CORE_FREQ/PWM_FREQ/0x10000 - 1;							// Specifies the prescaler value used to divide the TIM clock.
//	TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;			// Specifies the counter mode.
//	TIM_TimeBaseInitStructure.TIM_Period = 0x10000 - 1;						// 100 // Specifies the period value to be loaded into the active Auto-Reload Register at the next update event.
//	TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;				// Specifies the clock division.
//	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseInitStructure);						// Initializes the TIMx Time Base Unit peripheral according to the specified parameters in the TIM_TimeBaseInitStruct.

		/* MON1 */
//	TIM_OCStructInit(&TIM_OCInitStructure);									// Fills each TIM_OCInitStruct member with its default value.
//	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;						// Specifies the TIM mode.
//	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;			// Specifies the TIM Output Compare state.
//	TIM_OCInitStructure.TIM_Pulse = 0x10000/2 - 1;								// Specifies the pulse value to be loaded into the Capture Compare Register.
//	TIM_OC3Init(TIM2, &TIM_OCInitStructure);								// Initializes the TIMx Channel1 according to the specified parameters in the TIM_OCInitStruct.
//	TIM_OC3PreloadConfig(TIM2, TIM_OCPreload_Enable);						// Enables or disables the TIMx peripheral Preload register on CCR1.

//		/* MON2 */
//	TIM_OCStructInit(&TIM_OCInitStructure);									// Fills each TIM_OCInitStruct member with its default value.
//	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;						// Specifies the TIM mode.
//	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;			// Specifies the TIM Output Compare state.
//	TIM_OCInitStructure.TIM_Pulse = 0x10000/2 - 1;								// Specifies the pulse value to be loaded into the Capture Compare Register.
//	TIM_OC4Init(TIM2, &TIM_OCInitStructure);								// Initializes the TIMx Channel1 according to the specified parameters in the TIM_OCInitStruct.
//	TIM_OC4PreloadConfig(TIM2, TIM_OCPreload_Enable);						// Enables or disables the TIMx peripheral Preload register on CCR1.

//	TIM_CtrlPWMOutputs(TIM2, ENABLE);										// Enables or disables the TIM peripheral Main Outputs.
//	TIM_Cmd(TIM2, ENABLE);													// Enables or disables the specified TIM peripheral.

	/* TIM3 Configuration ------------------------------------------------- 1 ms timer */
//	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
//	TIM_DeInit(TIM3);
//	TIM_TimeBaseStructInit(&TIM_TimeBaseInitStructure);
//	TIM_TimeBaseInitStructure.TIM_Prescaler = (uint16_t)(CORE_FREQ / 1000);	// Specifies the prescaler value used to divide the TIM clock.
//	TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;			// Specifies the counter mode.
//	TIM_TimeBaseInitStructure.TIM_Period = 10 - 1;							// Specifies the period value to be loaded into the active Auto-Reload Register at the next update event.
//	TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;				// Specifies the clock division.
//	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseInitStructure);						// Initializes the TIMx Time Base Unit peripheral according to the specified parameters in the TIM_TimeBaseInitStruct.
//// 	TIM_Cmd(TIM3, ENABLE);													// Enables or disables the specified TIM peripheral.

// 	TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);								// Enables or disables the specified TIM interrupts.
// 	NVIC_EnableIRQ(TIM3_IRQn);
	
	SysTick_Config(CORE_FREQ/1000);
}

void Initialize_USART (void)
{
	USART_InitTypeDef USART_InitStructure;									// USART1 Conf Struct

	/* USART1 Configuration ------------------------------------------------ */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);					// Enables or disables the High Speed APB (APB2) peripheral clock.
	USART_DeInit(USART1);													// Deinitializes the USARTx peripheral registers to their default reset values.
	USART_StructInit(&USART_InitStructure);									// Fills each USART_InitStruct member with its default value.
	USART_InitStructure.USART_BaudRate = 460800;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_Init(USART1, &USART_InitStructure);								// Initializes the USARTx peripheral according to the specified parameters in the USART_InitStruct.
	USART_DMACmd(USART1, USART_DMAReq_Tx, ENABLE);							// Enables or disables the USARTs DMA interface.
	USART_Cmd(USART1, ENABLE);												// Enables or disables the specified USART peripheral.

// 	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);							// Enables or disables the specified USART interrupts. (Receive Data register not empty interrupt)
// 	NVIC_EnableIRQ(USART1_IRQn);
	
}

void Initialize_EXTI (void)
{
	GPIO_AFIODeInit();	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource0);
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource4);
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource5);
	
	EXTI_DeInit();
	EXTI_InitTypeDef EXTI_InitStructure;
	EXTI_StructInit(&EXTI_InitStructure);
	EXTI_InitStructure.EXTI_Line = EXTI_Line0 | EXTI_Line4 | EXTI_Line5;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
	EXTI_Init(&EXTI_InitStructure);

	NVIC_EnableIRQ(EXTI0_IRQn);
	NVIC_EnableIRQ(EXTI4_IRQn);
	NVIC_EnableIRQ(EXTI9_5_IRQn);
}


/* EOF */
