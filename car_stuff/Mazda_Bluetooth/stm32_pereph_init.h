/* Other peripheral hardware initialisation routine */
void Peripherals_Hardware_Startup (void);
void Initialize_Clocks (void);
void Initialize_Pins (void);
void Initialize_Timers (void);
void Initialize_ADC (void);
void Initialize_DAC (void);
void Initialize_DMA (void);
void Initialize_USART (void);
void Initialize_NVIC (void);
void Initialize_IWDG (void);
void Initialize_WWDG (void);
void Initialize_SPI (void);
void Initialize_EXTI (void);
