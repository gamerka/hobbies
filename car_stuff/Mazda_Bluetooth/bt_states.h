#include "stm32f10x.h"                  // Device header
/*
Статусы при подключенном питании:
оба выкл	-выключен
оба вкл на 1.5с через 1.5с после подачи сигнала -включился
попеременно T/2=0.4с через 2.3с после выключения обоих	-поиск
красный 2c	-выключение
синий мигает T/2=5c -подключен и играет
синий мигает T/2=2c -подключен и не играет
синий мигает 2 раза по 0.13с затем пауза 3.68с -собирается перейти  режим ожидания после 30с в режиме "подключен и не играет"
*/
#define LED_BLINK_MIN_PERIOD	100
#define LED_BLINK_MAX_PERIOD	5500
#define LED_BLINK_SEARCH_PERIOD 400
#define LED_BLINK_STANDBY_PERIOD 2000
#define LED_BLINK_PLAYING_PERIOD 5000
#define LED_TURNING_OFF_PERIOD 2000
#define MIN_POWER_ON_BOOT_TIME 6000

typedef enum {
	Bt_State_Unknown,
	Bt_State_Off,
	Bt_State_On,
	Bt_State_Searching,
	Bt_State_Playing,
	Bt_State_StandBy,
	Bt_State_LongStandBy,
	Bt_State_SwitchingOff
} BtState_TypeDef;



void UpdateBtState(void);
uint32_t BlueTimeSinceOn(void);
uint32_t BlueTimeSinceOff(void);
uint32_t RedTimeSinceOn(void);
uint32_t RedTimeSinceOff(void);
uint8_t CheckBlinkPeriodRough(uint32_t checkedValue, uint32_t expectedValue);

