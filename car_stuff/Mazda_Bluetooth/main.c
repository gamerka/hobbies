#include "stm32f10x.h"                  // Device header
#include "stm32_pereph_init.h"
#include "bt_states.h"

#define BLUE_LED_IN GPIOA, GPIO_Pin_4
#define RED_LED_IN GPIOA, GPIO_Pin_5
#define POWER_ON_BTN GPIOC, GPIO_Pin_8
#define AUDIO_ON_PIN GPIOC, GPIO_Pin_9
#define A0_BTN GPIOA, GPIO_Pin_0

#define BT_POWER_ON_PRESS_DELAY 1500

extern uint32_t sysClock;

extern BtState_TypeDef CurrentBtState;

extern uint8_t BlueState;
extern uint32_t blueOnTime;
extern uint32_t blueOffTime;
extern uint32_t BlueOnPeriodLast;

extern uint8_t RedState;
extern uint32_t redOnTime;
extern uint32_t redOffTime;
extern uint32_t RedOnPeriodLast;

extern uint32_t btPowerOnPressTime;

void PowerOn (void);
void AudioOn (void);
void AudioOff (void);
uint32_t millis (void);
void delay_ms (uint32_t t);

int main(void)
{
	Peripherals_Hardware_Startup();
	
	while(1) {
		BtState_TypeDef PreviousBtState;
		
		if (sysClock % 100)
		{
			UpdateBtState();
			if (CurrentBtState != PreviousBtState)
			{
				switch (CurrentBtState)
				{
					case Bt_State_Unknown:
						break;
					case Bt_State_Off:
						PowerOn();
						break;
					case Bt_State_On:
						break;
					case Bt_State_Searching:
						break;
					case Bt_State_Playing:
						AudioOn();
						break;
					case Bt_State_StandBy:
						AudioOn();
						break;
					case Bt_State_LongStandBy:
						break;
					case Bt_State_SwitchingOff:
						AudioOff();
						break;
				}
			}
			PreviousBtState = CurrentBtState;
		}
	}
}

inline void PowerOn (void)
{
	GPIO_SetBits(POWER_ON_BTN);
	btPowerOnPressTime = sysClock;
	delay_ms(BT_POWER_ON_PRESS_DELAY);
	GPIO_ResetBits(POWER_ON_BTN);
}

inline void AudioOn (void)
{
	GPIO_SetBits(AUDIO_ON_PIN);
}

inline void AudioOff (void)
{
	GPIO_ResetBits(AUDIO_ON_PIN);
}


void EXTI0_IRQHandler (void)
{
	if (GPIO_ReadInputDataBit(A0_BTN))
	{
		GPIO_SetBits(POWER_ON_BTN);
	}
	else
	{
		GPIO_ResetBits(POWER_ON_BTN);
	}
	EXTI_ClearITPendingBit(EXTI_Line0);
}


void EXTI4_IRQHandler (void)
{
	if (GPIO_ReadInputDataBit(BLUE_LED_IN))
	{
		BlueState = 1;
		blueOnTime = sysClock;
	}
	else
	{
		BlueState = 0;
		blueOffTime = sysClock;
		int t = (int)blueOffTime - (int)blueOnTime;
		BlueOnPeriodLast = (t < 0)?0:t;
	}
	//UpdateBtState();
	
	EXTI_ClearITPendingBit(EXTI_Line4);
}

void EXTI9_5_IRQHandler (void)
{
	if (GPIO_ReadInputDataBit(RED_LED_IN))
	{
		RedState = 1;
		redOnTime = sysClock;
	}
	else
	{
		RedState = 0;
		redOffTime = sysClock;
		int t = (int)redOffTime - (int)redOnTime;
		RedOnPeriodLast = (t < 0)?0:t;
	}
	//UpdateBtState();
	
	EXTI_ClearITPendingBit(EXTI_Line5);
}

void SysTick_Handler (void)
{
	sysClock++;
}

void delay_ms (uint32_t t)
{
  uint32_t start, end;
  start = millis();
  end = start + t;
  if (start < end) { 
    while ((millis() >= start) && (millis() < end)) { 
      // do nothing 
    } 
  } else { 
    while ((millis() >= start) || (millis() < end)) {
      // do nothing
    };
  }
}
inline uint32_t millis (void)
{
   return sysClock;
}
