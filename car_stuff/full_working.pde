
// http://compcar.ru  
#include <EEPROM.h>
//
//#define DEBUG 1
//
#define p_delay(VAR,MS) mills_poll_##VAR = millis()+MS
#define POLL(NAME)   if (mills_poll_##NAME<millis()) poll_##NAME()


//------------------------------
// назначение пинов ардуино
#define REVL_PIN      12    //вход ЛАМПОЧКИ заднего хода
#define AVSW_PIN      13    //выход на переключатель A/V для заднего хода
#define AMP_PIN       7     // Выход для включения усилителя

#define LIGHT_SENS   0 // Вход датчика освещености 
#define LIGHT_SENS_GRANDIS   1 // Вход с проводки автомобиля, отвечающей за подсветку, через делитель напряжения (1/2)  
#define LIGHT_SWITCH   2 // Вход с перемычки, ON = подсветка от автомобиля, OFF = автоматическое регулирование  
#define LIGHT_PIN   9 // Выход на транзистор регулировки яркости LED подсветки 


// -----------------------------
// AUTO_LIGHT 
#define AUTO_LIGHT_DELAY 3000 // миллисекунд, цикл проверки датчика освещения
//#define AUTO_LIGHT_DELAY_GRANDIS 100 // миллисекунд, цикл проверки проводки автомобиля
#define MAX_ULK 6    // сдвиг 3 вверх/3 вниз по градации яркости по "половинке" 
byte ulk=MAX_ULK/2; // коэффициент для ручной правки яркости
byte last_light;  
int sens;                // the average
#define SAVG_DELAY 3000 // задержка опроса датчика освещенности


// Rear Lamp
#define REVL_DELAY 500 // задержка на переключение на камеру заднего вида при включении задней передачи
#define REVL_DELAY_OFF 5000 // задержка на вЫключение камеры заднего вида

byte revl_state=0; // в задержке ли находится триггер заднего хода
#define REVL_STATE_OFFSET 10 // адрес eeprom для хранения состояния


// через сколько после старта включить усь
#define AMP_DELAY 1000  // 15 секунд задержка до включения усилителя

//---------------------------------- begin smooth
// Define the number of samples to keep track of.  The higher the number,
// the more the readings will be smoothed, but the slower the output will
// respond to the input.  Using a constant rather than a normal variable lets
// use this value to determine the size of the readings array.
//const int numReadings = 100;

//int readings[numReadings];      // the readings from the analog input
//int index = 0;                  // the index of the current reading
//int total = 0;                  // the running total
//int s_av = 0;                // the average
//------------------------ end smooth


// циклы опроса процедур
unsigned long mills_poll_REVL=0,
              mills_poll_AUTO_LIGHT=0,
              mills_poll_AMP=AMP_DELAY,
              mills_poll_SAVG=SAVG_DELAY;

// флаги
 
void setup(){  
  int cnt=0;
  int cnt1;  
  Serial.begin(115200);  
  revl_state=EEPROM.read(REVL_STATE_OFFSET);
  sens = analogRead(LIGHT_SENS)>>2; // установили первичное значение усредненного датчика освещенности

  
  pinMode(AVSW_PIN, OUTPUT);
  pinMode(LIGHT_PIN, OUTPUT); 
  pinMode(REVL_PIN, INPUT); 
  pinMode(LIGHT_SWITCH, INPUT); 

  digitalWrite(REVL_PIN, HIGH); //Подключить внутренний подтягивающий резистор 
//  digitalWrite(LIGHT_SWITCH, LOW); //Подключить внутренний подтягивающий резистор 
  digitalWrite(AVSW_PIN, LOW); 
  digitalWrite(AMP_PIN, LOW); 
  pinMode(AMP_PIN, OUTPUT);   

}  

void loop(){
  // если текущие миллисекунды больше интервала поллинга
POLL(SAVG);
POLL(AUTO_LIGHT);
//POLL(AMP);
POLL(REVL);
}


//------------------------------------------
// Включение усилителя
void poll_AMP(){ 
//  digitalWrite(AMP_PIN, HIGH);
//  analogWrite(AMP_PIN, 255);
//Serial.print(analogRead(AMP_PIN), DEC);
  
//  p_delay(AMP,1000); // задержка для следующего опроса -сутки
//  p_delay(AMP,3600*24*1000); // задержка для следующего опроса -сутки
}


void poll_SAVG(){ // среднее значение датчика освещенности

int sens_ = analogRead(LIGHT_SENS)>>2; //прочитали текущеее показание датчика

 if (sens_ > sens) {  // если текущее больше среднего - двигаем среднее вверх на одну позицию
    sens = sens + 1;
  } 

 if (sens_ < sens) {  // если текущее меньше среднего - двигаем среднее вниз на одну позицию
    sens = sens - 1;
  } 

//Serial.println(">>>*****");               
Serial.print(" sens_=");
Serial.print(sens_, DEC);
//Serial.print(" sens=");
Serial.print(" / ");
Serial.println(sens, DEC);               
Serial.println("*****");               

  p_delay(SAVG,SAVG_DELAY); // задержка для следующего опроса.

}

//--------------------------------
// Опрос автоматической корретировки света
// алгоритм работы взят у ув. CHIP-а (all-in-one project)
// изменения: сравнения с кратностью 30 заменены на сдвиг вправо на 5 битов (деление на 32)
//            введен сдвиговый коэфициент для корректировки яркости с пульта
//            свет=(свет>>4 + коэф_свет - МАКС_СВЕТ_КОЭФФ/2)
// для простоты показываю изменение яркости в винде от датчика и кнопок на пульте
// думаю, что будет удобно. если не настраивать кнопок работать будет по алгоритму CHIP
// \датчик| 0       32      64      128     160     192     224
// коэф \ |-----------------------------------------------------
// 0      | 2       2       2       64      96      128     160
// 1      | 2       2       64      96      128     160     192
// 2      | 2       2       64      96      128     160     192
// 3      | 2       64      96      128     160     192     255
// 4      | 2       64      96      128     160     192     255
// 5      | 64      96      128     160     192     255     255
// 6      | 64      96      128     160     192     255     255

void poll_AUTO_LIGHT(){ // управление яркостью
int light;   
byte light_grandis;   
// byte last_light;
// int AUTO_LIGHT_DELAY = 3000;
 // int sens = analogRead(LIGHT_SENS)>>2;   // /4 :) // интенсивность
//int sens = analogRead(LIGHT_SENS);

// sens=((sens>>4)+ulk-MAX_ULK/2)>>1;  // обработка коэффициента
// light=sens<=0?2:(sens>=6?255:(sens+1)<<5);


//int sens_grandis = analogRead(LIGHT_SENS_GRANDIS)>>2;
// int light_sw = analogRead(LIGHT_SWITCH)>>2;

//Serial.print(" sens=");
//Serial.println(sens, DEC);
//Serial.print(" sens_sred=");
//Serial.print(sens_sred, DEC);

//Serial.print(" insred=");
//Serial.print(InSrednee, DEC);


//Serial.print(" sensgrandis=");
//Serial.print(sens_grandis, DEC);

 
// sens=((sens>>4)+ulk-MAX_ULK/2)>>1;  // обработка коэффициента
// light=sens<=0?2:(sens>=6?255:(sens+1)<<5);


//регулировка по датчику освещенности------------
//if ((sens > 0) && (sens<=917)) {light = 1;}
//if ((sens > 917) && (sens<=918)) {light = 5;}
//if ((sens > 918) && (sens<=920)) {light = 10;}
//if ((sens > 920) && (sens<=970)) {light = 15;}
//if ((sens > 970) && (sens<=975)) {light = 80;}
//if ((sens > 975) && (sens<=980)) {light = 100;}
//if ((sens > 980) && (sens<=990)) {light = 130;}
//if ((sens > 990) && (sens<=1000)) {light = 160;}
//if ((sens > 1000) && (sens<=1010)) {light = 200;}
//if ((sens > 1010) && (sens<=1020)) {light = 230;}
//if (sens > 1020) {light = 255;}
//-------------------
if ((sens > 0) && (sens<=5)) {light = 1;}   
if ((sens > 5) && (sens<=10)) {light = 2;}   
if ((sens > 10) && (sens<=15)) {light = 3;}
if ((sens > 15) && (sens<=20)) {light = 4;}
if ((sens > 20) && (sens<=30)) {light = 5;}
if ((sens > 30) && (sens<=40)) {light = 6;}
if ((sens > 40) && (sens<=50)) {light = 7;}
if ((sens > 50) && (sens<=60)) {light = 8;}
if ((sens > 60) && (sens<=80)) {light = 9;}
if ((sens > 80) && (sens<=100)) {light = 10;}
if ((sens > 100) && (sens<=120)) {light = 15;}
if ((sens > 120) && (sens<=140)) {light = 20;}
if ((sens > 140) && (sens<=170)) {light = 30;}   
if ((sens > 170) && (sens<=190)) {light = 40;}   
if ((sens > 190) && (sens<=200)) {light = 50;}
if ((sens > 200) && (sens<=210)) {light = 70;}
if ((sens > 210) && (sens<=215)) {light = 90;}
if ((sens > 215) && (sens<=220)) {light = 100;}
if ((sens > 220) && (sens<=225)) {light = 200;}
//if ((sens > 225) && (sens<=230)) {light = 150;}
//if ((sens > 230) && (sens<=232)) {light = 200;}

if (sens > 225)  {light = 255;}   



// регулировка из проводки автомобиля
//if ((sens_grandis > 10) && (sens_grandis<100)) {light_grandis = 255;}
//if ((sens_grandis > 100) && (sens_grandis<150)) {light_grandis = 150;}
//if ((sens_grandis > 150) && (sens_grandis<200)) {light_grandis = 50;}
//if ((sens_grandis > 200) && (sens_grandis<254)) {light_grandis = 10;}
//-----------------------

// проверяем включена ли перемычка
//  if (analogRead(LIGHT_SWITCH) < 1000) {
//    light = light_grandis;
//    AUTO_LIGHT_DELAY = 1000;
//  }

//Serial.print(" sw=");
//Serial.print(analogRead(LIGHT_SWITCH), DEC);

//Serial.print(" del=");
//Serial.print(AUTO_LIGHT_DELAY, DEC);
 
//Serial.print(" light=");
//Serial.print(light, DEC);


 //if (light!=last_light) {
 //   analogWrite(LIGHT_PIN, light);
 //   last_light=light;
 // }
//int fadeValue;
//fadeValue = last_light;

//Serial.print(" last_light=");
//Serial.println(last_light, DEC);

Serial.print(" light=");
Serial.println(light, DEC);

//light = light * 100;
//last_light = last_light * 100;

 if (light >  last_light) {
 
 // fade in from min to max in increments of 1 points:

  for(int fadeValue = last_light; fadeValue <= light; fadeValue +=1) { 
    // sets the value (range from 0 to 255):
//    fadeValue=fadeValue / 100;
    analogWrite(LIGHT_PIN, fadeValue);         
    // wait for 30 milliseconds to see the dimming effect    
    delay(30);                            
  } 
 }
 
 if (light < last_light) {
 
 // fade in from max to min in increments of 1 points:
  for(int fadeValue = last_light; fadeValue >= light; fadeValue -=1) { 
    // sets the value (range from 0 to 255):
    analogWrite(LIGHT_PIN, fadeValue);         
    // wait for 30 milliseconds to see the dimming effect    
    delay(30);                            
  } 
 }
  
last_light=light;
//Serial.print(" last_light=");
//Serial.print(last_light, DEC);
 
  p_delay(AUTO_LIGHT,AUTO_LIGHT_DELAY); // задержка для следующего опроса.
}

//--------------------------------
// обработка переключения моника на камеру заднего вида
// задача не требуется обработки в реальном времени, 
// поэтому упрощаем алгоритм до срабатывания 1 раз в REVL_DELAY мс
void poll_REVL(){ 
byte temp;
temp=revl_state;
  if (digitalRead(REVL_PIN) != HIGH){
    switch (revl_state) {
      case 0:
        revl_state=1;
        break;
      case 1:
        revl_state=2;
        digitalWrite(AVSW_PIN, HIGH);  // подать сигнал на включение камеры  
    }
  } else {
    if (revl_state==2) {
      delay(REVL_DELAY_OFF);
      digitalWrite(AVSW_PIN, LOW);  // снять сигнал 
    }
   
    revl_state=0;
   
  }
   p_delay(REVL,REVL_DELAY); // задержка для следующего опроса.

if (temp!=revl_state){
    EEPROM.write(REVL_STATE_OFFSET,revl_state);
}
}


