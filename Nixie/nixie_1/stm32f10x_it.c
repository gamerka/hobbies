/**
  ******************************************************************************
  * @file    stm32f10x_it.c 
  * @author  MCD Application Team
  * @version V3.5.0
  * @date    08-April-2011
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and 
  *          peripherals interrupt service routine.
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x_it.h"
#include <stdio.h>
#include <time.h>

/** @addtogroup STM32F10x_StdPeriph_Template
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
struct UART_Struct UART_RX;
char NMEA_Buff[60];
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
struct tm* Time_str;
time_t Time_raw;
/* Private function prototypes -----------------------------------------------*/
void NMEA_Parse (void);
void ShowTime (char Hours, char Minutes, char Seconds);
/* Private functions ---------------------------------------------------------*/
void USART1_IRQHandler (void) {																			// Text user interface handler
	int i = 0;	
	char InpChr = 0;
	if (USART1->SR & USART_SR_PE || USART1->SR & USART_SR_NE || USART1->SR & USART_SR_FE) {				// USART error handling
		;
	}
	if (USART1->SR & USART_SR_ORE) { 																	// It is cleared by a software sequence (a read to the
		;																								// USART_SR register followed by a read to the USART_DR register).
	}	
	InpChr = USART1->DR; 																				// ?????? ????????? ??????

	if (InpChr == '$')															// �������� �����
	{
		UART_RX.RX_Cnt = 0;
		
		
		
	}
	else if (InpChr == '\r')													// ����������� �����
	{
		UART_RX.RX_Ready_to_Parse = 1;
		for (char i = UART_RX.RX_Cnt; i < sizeof(NMEA_Buff); i++) NMEA_Buff[i] = 0;		// ������ ������
		NMEA_Parse();
			
		
	}
	else if (UART_RX.RX_Ready_to_Parse == 0)									// ���� ����� �� �������
	{
		NMEA_Buff[UART_RX.RX_Cnt] = InpChr;
		UART_RX.RX_Cnt++;
		
	}
	
}

void RTC_IRQHandler (void)
{
	PWR_BackupAccessCmd(ENABLE);
	RTC_ClearITPendingBit(RTC_IT_SEC);
	RTC_ClearFlag(RTC_FLAG_SEC);
	PWR_BackupAccessCmd(DISABLE);

	
}
/**
  * @}
  */ 

/******************************************************************************/
/*            Cortex-M3 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{
	if(!(GPIO_ReadOutputDataBit(GREEN_LED))) GPIO_SetBits(GREEN_LED);
	else GPIO_ResetBits(GREEN_LED);
	
	Time_str->tm_sec++;
// 	if ((Time_raw == 0) || (Time_str->tm_sec >= 60))								// ��� � ������ ��������� ����� �� RTC
// 	{
// 		RTC_WaitForSynchro();
// 		Time_raw = RTC_GetCounter();
// 		Time_str = localtime(&Time_raw);
// 	}
		
	ShowTime(Time_str->tm_sec, Time_str->tm_min, 0);	
	
}

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval None
  */
void PendSV_Handler(void)
{
}


/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
