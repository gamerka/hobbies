/* config.h */
#define GREEN_LED	GPIOC, GPIO_Pin_9
#define BLUE_LED	GPIOC, GPIO_Pin_8

#define H_h_A	GPIOC, GPIO_Pin_0
#define H_h_B	GPIOC, GPIO_Pin_1
#define H_h_C	GPIOC, GPIO_Pin_2
#define H_h_D	GPIOC, GPIO_Pin_3
#define H_l_A	GPIOA, GPIO_Pin_1
#define H_l_B	GPIOA, GPIO_Pin_2
#define H_l_C	GPIOA, GPIO_Pin_3
#define H_l_D	GPIOA, GPIO_Pin_4
#define M_h_A	GPIOA, GPIO_Pin_5
#define M_h_B	GPIOA, GPIO_Pin_6
#define M_h_C	GPIOA, GPIO_Pin_7
#define M_h_D	GPIOC, GPIO_Pin_4
#define M_l_A	GPIOC, GPIO_Pin_5
#define M_l_B	GPIOB, GPIO_Pin_0
#define M_l_C	GPIOB, GPIO_Pin_1
#define M_l_D	GPIOB, GPIO_Pin_2
#define S_h_A	GPIOB, GPIO_Pin_10
#define S_h_B	GPIOB, GPIO_Pin_11
#define S_h_C	GPIOB, GPIO_Pin_12
#define S_h_D	GPIOB, GPIO_Pin_13
#define S_l_A	GPIOB, GPIO_Pin_14
#define S_l_B	GPIOB, GPIO_Pin_15
#define S_l_C	GPIOC, GPIO_Pin_6
#define S_l_D	GPIOC, GPIO_Pin_7

#define MSK_ZONE	4

struct UART_Struct {
	char Checksum_RX;
	char RX_Ready_to_Parse;
	char RX_Cnt;
};

struct NMEA_Struct {
	char GLL_OK;
	char Comma_Cnt;
	char Time_Start;
	char Time_Str[10];
	int Time;
};

