/**
  ******************************************************************************
  * @file    stm32_pereph_init.c
  * @brief   Main perepheral initialization routines
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"
#include "stm32_pereph_init.h"
#include "config.h"

extern uint8_t OW_RX;
extern uint8_t OW_TX;

void Peripherals_Hardware_Startup(void)
{
	Initialize_Clocks();
	Initialize_Pins();
	Initialize_Timers();
	Initialize_USART();
// 	Initialize_DMA();
	Initialize_RTC();
// 	Initialize_IWDG();

}

void Initialize_Clocks (void)
{
	/* RCC Init ----------------------------------------------------------- */
	RCC_DeInit();															// Resets the RCC clock configuration to the default reset state.
	RCC_HSEConfig(RCC_HSE_ON);												// Configures the External High Speed oscillator (HSE).
	while (RCC_WaitForHSEStartUp() == ERROR);								// Waits for HSE start-up.
	RCC_HCLKConfig(RCC_SYSCLK_Div1);										// Configures the AHB clock (HCLK).
	RCC_PCLK1Config(RCC_HCLK_Div2);											// Configures the Low Speed APB clock (PCLK1).
	RCC_PCLK2Config(RCC_HCLK_Div1);											// Configures the High Speed APB clock (PCLK2).
#ifdef STM32F10X_CL
	#define CORE_FREQ	72000000
	RCC_PREDIV2Config(RCC_PREDIV2_Div5);									// Configures the PREDIV2 division factor. (25Mhz/5=5Mhz)
	RCC_PLL2Config(RCC_PLL2Mul_8);											// Configures the PLL2 multiplication factor. (5Mhz*8=40Mhz)
	RCC_PLL2Cmd(ENABLE);													// Enables or disables the PLL2.
	RCC_PREDIV1Config(RCC_PREDIV1_Source_PLL2, RCC_PREDIV1_Div5);			// Configures the PREDIV1 division factor. (40Mhz/5=8Mhz)
	RCC_PLLConfig(RCC_PLLSource_PREDIV1, RCC_PLLMul_9);						// Configures the PLL clock source and multiplication factor. (8Mhz*9=72Mhz)
	RCC_ADCCLKConfig(RCC_PCLK2_Div6);										/* ADCCLK = PCLK2/2 = 24/2 = 12MHz*/ // Or 72/6 for 72MHz

	FLASH_PrefetchBufferCmd(FLASH_PrefetchBuffer_Enable);					// Enables or disables the Prefetch Buffer.
	FLASH_SetLatency(FLASH_Latency_2);										// Sets the code latency value.
#elif defined STM32F10X_MD_VL
	#define CORE_FREQ	24000000
	RCC_PLLConfig(RCC_PLLSource_PREDIV1, RCC_PLLMul_3);						// Configures the PLL clock source and multiplication factor. (9 for 72Mhz, 3 for 24Mhz)
	RCC_ADCCLKConfig(RCC_PCLK2_Div2);										/* ADCCLK = PCLK2/2 = 24/2 = 12MHz*/ // Or 72/6 for 72MHz
#else
	#define CORE_FREQ	72000000
	RCC_PLLConfig(RCC_PLLSource_HSE_Div1, RCC_PLLMul_9);					// Configures the PLL clock source and multiplication factor. (9 for 72Mhz, 3 for 24Mhz)
	RCC_ADCCLKConfig(RCC_PCLK2_Div6);										/* ADCCLK = PCLK2/2 = 24/2 = 12MHz*/ // Or 72/6 for 72MHz

	FLASH_PrefetchBufferCmd(FLASH_PrefetchBuffer_Enable);					// Enables or disables the Prefetch Buffer.
	FLASH_SetLatency(FLASH_Latency_2);										// Sets the code latency value.
#endif
	RCC_PLLCmd(ENABLE);														// Enables or disables the PLL.
	RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);								// Configures the system clock (SYSCLK).

}

void Initialize_Pins (void)
{
	GPIO_InitTypeDef GPIO_InitStructureCore_Hardware_Startup;
	GPIO_InitTypeDef GPIO_InitStructure;									// GPIO Conf Struct

	/* Configure all unused GPIO port pins in Analog Input mode (floating input
	 trigger OFF), this will reduce the power consumption and increase the device
	 immunity against EMI/EMC  */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB |
	RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOD, ENABLE);
	GPIO_InitStructureCore_Hardware_Startup.GPIO_Pin = GPIO_Pin_All;
	GPIO_InitStructureCore_Hardware_Startup.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_Init(GPIOA, &GPIO_InitStructureCore_Hardware_Startup);
	GPIO_Init(GPIOB, &GPIO_InitStructureCore_Hardware_Startup);
	GPIO_Init(GPIOC, &GPIO_InitStructureCore_Hardware_Startup);
	GPIO_Init(GPIOD, &GPIO_InitStructureCore_Hardware_Startup);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB |
	RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOD, DISABLE);

 	/* GPIO Init ---------------------------------------------------------- */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | 
	RCC_APB2Periph_GPIOC | RCC_APB2Periph_AFIO, ENABLE);					// Enables or disables the High Speed APB (APB2) peripheral clock.

		/* LED pins */
	GPIO_StructInit(&GPIO_InitStructure);									// Fills each GPIO_InitStruct member with its default value.
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 						// Specifies the speed for the selected pins.
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;						// Specifies the operating mode for the selected pins.
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9;					// Specifies the GPIO pins to be configured.
	GPIO_Init(GPIOC, &GPIO_InitStructure);									// Initializes the GPIOx peripheral according to the specified parameters in the GPIO_InitStruct.

// 		/* USART1 pins */
// 	GPIO_StructInit(&GPIO_InitStructure);									// Fills each GPIO_InitStruct member with its default value.
// 	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 						// Specifies the speed for the selected pins.
// 	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;							// Specifies the operating mode for the selected pins.
// 	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;								// Specifies the GPIO pins to be configured.
// 	GPIO_Init(GPIOA, &GPIO_InitStructure);									// Initializes the GPIOx peripheral according to the specified parameters in the GPIO_InitStruct.

// 	GPIO_StructInit(&GPIO_InitStructure);									// Fills each GPIO_InitStruct member with its default value.
// 	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;					// Specifies the operating mode for the selected pins.
// 	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;								// Specifies the GPIO pins to be configured.
// 	GPIO_Init(GPIOA, &GPIO_InitStructure);									// Initializes the GPIOx peripheral according to the specified parameters in the GPIO_InitStruct.

// 		/* Digital IN */
// 	GPIO_StructInit(&GPIO_InitStructure);									// Fills each GPIO_InitStruct member with its default value.
// 	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 						// Specifies the speed for the selected pins.
// 	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;							// Specifies the operating mode for the selected pins.
// 	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_0;					// Specifies the GPIO pins to be configured.
// 	GPIO_Init(GPIOA, &GPIO_InitStructure);									// Initializes the GPIOx peripheral according to the specified parameters in the GPIO_InitStruct.
// 		
		/* Digits digital OUT */
	GPIO_StructInit(&GPIO_InitStructure);									// Fills each GPIO_InitStruct member with its default value.
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 						// Specifies the speed for the selected pins.
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;						// Specifies the operating mode for the selected pins.
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7;
	GPIO_Init(GPIOA, &GPIO_InitStructure);									// Initializes the GPIOx peripheral according to the specified parameters in the GPIO_InitStruct.
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_Init(GPIOB, &GPIO_InitStructure);									// Initializes the GPIOx peripheral according to the specified parameters in the GPIO_InitStruct.
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7;
	GPIO_Init(GPIOC, &GPIO_InitStructure);									// Initializes the GPIOx peripheral according to the specified parameters in the GPIO_InitStruct.
	
// 		/* Temp sens in */
// 	GPIO_StructInit(&GPIO_InitStructure);									// Fills each GPIO_InitStruct member with its default value.
// 	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 						// Specifies the speed for the selected pins.
// 	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;					// Specifies the operating mode for the selected pins.
// 	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;								// Specifies the GPIO pins to be configured.
// 	GPIO_Init(GPIOB, &GPIO_InitStructure);									// Initializes the GPIOx peripheral according to the specified parameters in the GPIO_InitStruct.

// 		/* Temp sens out */
// 	GPIO_SetBits(TEMP_OUT);	// Hi-Z
// 	GPIO_StructInit(&GPIO_InitStructure);									// Fills each GPIO_InitStruct member with its default value.
// 	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 						// Specifies the speed for the selected pins.
// 	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;						// Specifies the operating mode for the selected pins.
// 	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
// 	GPIO_Init(GPIOB, &GPIO_InitStructure);									// Initializes the GPIOx peripheral according to the specified parameters in the GPIO_InitStruct.
// 	GPIO_SetBits(TEMP_OUT);	// Hi-Z
	
		/* USART1 pins */
	GPIO_PinRemapConfig(GPIO_Remap_USART1, ENABLE);
	GPIO_StructInit(&GPIO_InitStructure);									// Fills each GPIO_InitStruct member with its default value.
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 						// Specifies the speed for the selected pins.
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;							// Specifies the operating mode for the selected pins.
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;								// Specifies the GPIO pins to be configured.
	GPIO_Init(GPIOB, &GPIO_InitStructure);									// Initializes the GPIOx peripheral according to the specified parameters in the GPIO_InitStruct.

	GPIO_StructInit(&GPIO_InitStructure);									// Fills each GPIO_InitStruct member with its default value.
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;					// Specifies the operating mode for the selected pins.
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;								// Specifies the GPIO pins to be configured.
	GPIO_Init(GPIOB, &GPIO_InitStructure);									// Initializes the GPIOx peripheral according to the specified parameters in the GPIO_InitStruct.

}

void Initialize_Timers (void)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;						// TIM Conf Struct
// 	TIM_OCInitTypeDef TIM_OCInitStructure;									// TIM OC Conf Struct

	/* SysTick Configuration ------------------------------------------------*/
	SysTick_Config(CORE_FREQ/2);											// 0.5 s

	/* TIM2 Configuration ------------------------------------------------ For Out PWM */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	TIM_DeInit(TIM2);
	TIM_TimeBaseStructInit(&TIM_TimeBaseInitStructure);
	TIM_TimeBaseInitStructure.TIM_Prescaler = CORE_FREQ/1000 - 1;	// Specifies the prescaler value used to divide the TIM clock.
	TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;			// Specifies the counter mode.
	TIM_TimeBaseInitStructure.TIM_Period = 1 - 1;						// 100 // Specifies the period value to be loaded into the active Auto-Reload Register at the next update event.
	TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;				// Specifies the clock division.
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseInitStructure);						// Initializes the TIMx Time Base Unit peripheral according to the specified parameters in the TIM_TimeBaseInitStruct.
	
}

void Initialize_DMA (void)
{
	DMA_InitTypeDef DMA_InitStructure; 										// Variable used to setup the DMA
	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);						/* DMA1 clock enable */

	/* USART1_TX DMA */
	DMA_DeInit(DMA1_Channel4); 												// Set DMA registers to default values
	DMA_StructInit(&DMA_InitStructure);										// Fills each DMA_InitStruct member with its default value.

	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &USART1->DR;		// Address of peripheral the DMA must map to
 	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) &OW_TX;	 			// Variable to which ADC values will be stored
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;						// Specifies if the peripheral is the source or destination.
	DMA_InitStructure.DMA_BufferSize = 1; 									// Specifies the buffer size, in data unit, of the specified Channel.
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;		// Specifies whether the Peripheral address register is incremented or not.
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;					// Specifies whether the memory address register is incremented or not.
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;	// Specifies the Peripheral data width.
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;			// Specifies the Memory data width.
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;							// Specifies the operation mode of the DMAy Channelx.
	DMA_InitStructure.DMA_Priority = DMA_Priority_Low;						// Specifies the software priority for the DMAy Channelx.
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;							// Specifies if the DMAy Channelx will be used in memory-to-memory transfer.
	
	DMA_Init(DMA1_Channel4, &DMA_InitStructure); 							// Initialize the DMA
	//DMA_Cmd(DMA1_Channel4, ENABLE); 										// Enable the DMA1 - Channel4

	/* USART1_RX DMA */
	DMA_DeInit(DMA1_Channel5); 												// Set DMA registers to default values
	DMA_StructInit(&DMA_InitStructure);										// Fills each DMA_InitStruct member with its default value.

	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &USART1->DR;		// Address of peripheral the DMA must map to
 	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) &OW_RX; 			// Variable to which ADC values will be stored
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;						// Specifies if the peripheral is the source or destination.
	DMA_InitStructure.DMA_BufferSize = 1; 									// Specifies the buffer size, in data unit, of the specified Channel.
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;		// Specifies whether the Peripheral address register is incremented or not.
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;					// Specifies whether the memory address register is incremented or not.
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;	// Specifies the Peripheral data width.
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;			// Specifies the Memory data width.
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;							// Specifies the operation mode of the DMAy Channelx.
	DMA_InitStructure.DMA_Priority = DMA_Priority_Low;						// Specifies the software priority for the DMAy Channelx.
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;							// Specifies if the DMAy Channelx will be used in memory-to-memory transfer.
	
	DMA_Init(DMA1_Channel5, &DMA_InitStructure); 							// Initialize the DMA

}

void Initialize_USART (void)
{
	USART_InitTypeDef USART_InitStructure;									// USART1 Conf Struct

	/* USART1 Configuration ------------------------------------------------ */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);					// Enables or disables the High Speed APB (APB2) peripheral clock.
	USART_DeInit(USART1);													// Deinitializes the USARTx peripheral registers to their default reset values.
	USART_StructInit(&USART_InitStructure);									// Fills each USART_InitStruct member with its default value.
	USART_InitStructure.USART_BaudRate = 57600;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_Init(USART1, &USART_InitStructure);								// Initializes the USARTx peripheral according to the specified parameters in the USART_InitStruct.
// 	USART_DMACmd(USART1, USART_DMAReq_Tx, ENABLE);							// Enables or disables the USARTs DMA interface.
	USART_Cmd(USART1, ENABLE);												// Enables or disables the specified USART peripheral.

	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);							// Enables or disables the specified USART interrupts. (Receive Data register not empty interrupt)
	NVIC_EnableIRQ(USART1_IRQn);
	
}

void Initialize_RTC(void)
{
	/* Enable PWR and BKP clocks */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);
	
	if (BKP_ReadBackupRegister(BKP_DR1) != 0xA5A5)
	{
		/* Backup data register value is not correct or not yet programmed (when
		   the first time the program is executed) */
		/* RTC Configuration */
		GPIO_SetBits(BLUE_LED);
		
		/* Allow access to BKP Domain */
		PWR_BackupAccessCmd(ENABLE);
		/* Reset Backup Domain */
		BKP_DeInit();
		/* Enable LSE */
		RCC_LSEConfig(RCC_LSE_ON);
		/* Wait till LSE is ready */
		while (RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET);
		/* Select LSE as RTC Clock Source */
		RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);
		/* Sets the RTC counter value */
		RTC_SetCounter(0);
		/* Enable RTC Clock */
		RCC_RTCCLKCmd(ENABLE);
		/* Wait for RTC registers synchronization */
		RTC_WaitForSynchro();
		/* Wait until last write operation on RTC registers has finished */
		RTC_WaitForLastTask();
		/* Enable the RTC Second */
// 		RTC_ITConfig(RTC_IT_SEC, ENABLE);
		/* Wait until last write operation on RTC registers has finished */
		RTC_WaitForLastTask();
		/* Set RTC prescaler: set RTC period to 1sec */
		RTC_SetPrescaler(32767); /* RTC period = RTCCLK/RTC_PR = (32.768 KHz)/(32767+1) */
		/* Wait until last write operation on RTC registers has finished */
		RTC_WaitForLastTask();

		BKP_WriteBackupRegister(BKP_DR1, 0xA5A5);

		PWR_BackupAccessCmd(DISABLE);

	}
	else
	{
		/* Wait for RTC registers synchronization */
		RTC_WaitForSynchro();
		/* Enable the RTC Second */
// 		RTC_ITConfig(RTC_IT_SEC, ENABLE);
		/* Wait until last write operation on RTC registers has finished */
		RTC_WaitForLastTask();
	}

// 	NVIC_EnableIRQ(RTC_IRQn);
}

void Initialize_IWDG (void)
{
	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);							// Enables or disables write access to IWDG_PR and IWDG_RLR registers.
	IWDG_SetPrescaler(IWDG_Prescaler_4);									// Sets IWDG Prescaler value. (4-256)
	IWDG_SetReload(200);													// Тактируется от LSI 40кГц. (40000 / 4 / 200 = 20мс)
																			// Sets IWDG Reload value. This parameter must be a number between 0 and 0x0FFF.
	IWDG_Enable();															// Enables IWDG (write access to IWDG_PR and IWDG_RLR registers disabled).
}

/* EOF */
