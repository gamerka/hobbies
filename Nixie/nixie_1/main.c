
/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
// #include <math.h>
#include <time.h>
#include "stm32_pereph_init.h"
#include "config.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
char Receive[10];
char OW_RX [8];
char OW_TX [8];
struct NMEA_Struct NMEA;
/* Private function prototypes -----------------------------------------------*/
void Wait_ms (uint16_t wait);
char OW_USART_Reset (void);
char OW_USART_Write (const char* Send, char* Receive, int Length, int Shift);
float GetTemp (void);
void ShowTime (char Hours, char Minutes, char Seconds);
/* Private functions ---------------------------------------------------------*/
int main (void)
{
	Peripherals_Hardware_Startup();
	
	while (1)
	{

	}
}

char OW_USART_Write (const char* Send, char* Receive, int Length, int Shift)
{
	char RecI = 0;
	for (char i = 0; i < Length; i++)
	{
		for (char j = 0; j < 8; j++)
		{
			if ((Send[i] >> j) & 1) OW_TX[j] = 0xff;
			else OW_TX[j] = 0;
		}
	
		DMA_SetCurrDataCounter(DMA1_Channel4, 8);
		DMA_SetCurrDataCounter(DMA1_Channel5, 8);
		USART_ClearFlag(USART1, USART_FLAG_RXNE | USART_FLAG_TC | USART_FLAG_TXE);
		USART_DMACmd(USART1, USART_DMAReq_Tx | USART_DMAReq_Rx, ENABLE);
		DMA_Cmd(DMA1_Channel5, ENABLE);
		DMA_Cmd(DMA1_Channel4, ENABLE);
		
		while (DMA_GetFlagStatus(DMA1_FLAG_TC5) == RESET){};

		DMA_Cmd(DMA1_Channel4, DISABLE);
		DMA_Cmd(DMA1_Channel5, DISABLE);
		USART_DMACmd(USART1, USART_DMAReq_Tx | USART_DMAReq_Rx, DISABLE);
		DMA_ClearFlag(DMA1_FLAG_TC5);
			
		if ((Receive != NULL) && (i >= Shift))
		{
			Receive[RecI] = 0;
			for (char j = 0; j < 8; j++)
			{
				if (OW_RX[j] == 0xFF) Receive[RecI] |= 1 << j;
			}
			RecI++;
		}
	}
	
	return 0;
}

char OW_USART_Reset (void)
{
	USART_InitTypeDef USART_InitStructure;									// USART1 Conf Struct
	
	USART_DMACmd(USART1, USART_DMAReq_Tx | USART_DMAReq_Rx, DISABLE);		// Enables or disables the USARTs DMA interface.

	USART_StructInit(&USART_InitStructure);									// Fills each USART_InitStruct member with its default value.
	USART_InitStructure.USART_BaudRate = 9600;
	USART_Init(USART1, &USART_InitStructure);								// Initializes the USARTx peripheral according to the specified parameters in the USART_InitStruct.

	USART_ClearFlag(USART1, USART_FLAG_TC);
	USART_SendData(USART1, 0xf0);
	while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
	uint8_t Presence = USART_ReceiveData(USART1);

	USART_StructInit(&USART_InitStructure);									// Fills each USART_InitStruct member with its default value.
	USART_InitStructure.USART_BaudRate = 115200;
	USART_Init(USART1, &USART_InitStructure);								// Initializes the USARTx peripheral according to the specified parameters in the USART_InitStruct.
	
	if (Presence != 0xf0) {
		return 1;
	}
	return 0;
}

void Wait_ms (uint16_t wait)
{
	TIM_SetAutoreload(TIM2, wait - 1);
	TIM_ClearFlag(TIM2, TIM_FLAG_Update);
	TIM_Cmd(TIM2, ENABLE);
	while (!(TIM_GetFlagStatus(TIM2, TIM_FLAG_Update)));
	TIM_Cmd(TIM2, DISABLE);
}

float GetTemp (void)
{
	float Temp = 0;
	OW_USART_Reset();
	OW_USART_Write("\xcc\x44", NULL, 2, 0);
	Wait_ms(750);
	OW_USART_Reset();
	OW_USART_Write("\xcc\xbe\xff\xff\xff\xff\xff\xff\xff\xff", Receive, 10, 2);
	Temp = Receive[0]/2;
	if (Receive[1] == 0xFF) Temp = -Temp;
	Temp = Temp - 0.25 + (float)(Receive[7] - Receive[6]) / Receive[7];

	return Temp;
}	

void ShowTime (char Hours, char Minutes, char Seconds)
{

	/* Hours */
	GPIO_WriteBit(H_h_A, (BitAction)(((Hours / 10) >> 0) & 1));
	GPIO_WriteBit(H_h_B, (BitAction)(((Hours / 10) >> 1) & 1));
	GPIO_WriteBit(H_h_C, (BitAction)(((Hours / 10) >> 2) & 1));
	GPIO_WriteBit(H_h_D, (BitAction)(((Hours / 10) >> 3) & 1));
	
	GPIO_WriteBit(H_l_A, (BitAction)(((Hours % 10) >> 0) & 1));
	GPIO_WriteBit(H_l_B, (BitAction)(((Hours % 10) >> 1) & 1));
	GPIO_WriteBit(H_l_C, (BitAction)(((Hours % 10) >> 2) & 1));
	GPIO_WriteBit(H_l_D, (BitAction)(((Hours % 10) >> 3) & 1));
	
	/* Minutes */
	GPIO_WriteBit(M_h_A, (BitAction)(((Minutes / 10) >> 0) & 1));
	GPIO_WriteBit(M_h_B, (BitAction)(((Minutes / 10) >> 1) & 1));
	GPIO_WriteBit(M_h_C, (BitAction)(((Minutes / 10) >> 2) & 1));
	GPIO_WriteBit(M_h_D, (BitAction)(((Minutes / 10) >> 3) & 1));
	
	GPIO_WriteBit(M_l_A, (BitAction)(((Minutes % 10) >> 0) & 1));
	GPIO_WriteBit(M_l_B, (BitAction)(((Minutes % 10) >> 1) & 1));
	GPIO_WriteBit(M_l_C, (BitAction)(((Minutes % 10) >> 2) & 1));
	GPIO_WriteBit(M_l_D, (BitAction)(((Minutes % 10) >> 3) & 1));
	
	/* Seconds */
	GPIO_WriteBit(S_h_A, (BitAction)(((Seconds / 10) >> 0) & 1));
	GPIO_WriteBit(S_h_B, (BitAction)(((Seconds / 10) >> 1) & 1));
	GPIO_WriteBit(S_h_C, (BitAction)(((Seconds / 10) >> 2) & 1));
	GPIO_WriteBit(S_h_D, (BitAction)(((Seconds / 10) >> 3) & 1));
	
	GPIO_WriteBit(S_l_A, (BitAction)(((Seconds % 10) >> 0) & 1));
	GPIO_WriteBit(S_l_B, (BitAction)(((Seconds % 10) >> 1) & 1));
	GPIO_WriteBit(S_l_C, (BitAction)(((Seconds % 10) >> 2) & 1));
	GPIO_WriteBit(S_l_D, (BitAction)(((Seconds % 10) >> 3) & 1));

}

void NMEA_Parse (void)
{
	/* $GPGLL,5549.04728,N,03731.95186,E,071318.00,A,A*64<CR><LF> */
	extern char NMEA_Buff[60];
	extern struct UART_Struct UART_RX;
	extern struct tm* Time_str;
	time_t Time_raw;
	char Temp1[6] = "GPGLL", Temp2[6];
	strncpy(Temp2, NMEA_Buff, 5);
	if (strncmp(Temp1, Temp2, 5) == 0)
	{
		for (char i = 5; i < sizeof(NMEA_Buff); i++)
		{
			if (NMEA_Buff[i] == ',')
			{
				NMEA.Comma_Cnt++;
				if (NMEA.Comma_Cnt == 5)
				{
					NMEA.Time_Start = i+1;
					break;
				}
			}
		}
		if (NMEA_Buff[NMEA.Time_Start] != ',')
		{
			strncpy(NMEA.Time_Str, &NMEA_Buff[NMEA.Time_Start], 9);
			NMEA.Time = atoi(NMEA.Time_Str);
			Time_str->tm_sec = NMEA.Time%100;
			Time_str->tm_min = (NMEA.Time/100)%100;
			Time_str->tm_hour = (NMEA.Time/10000 + MSK_ZONE)%24;
			Time_raw = mktime(Time_str);
			
			NVIC_DisableIRQ(RTC_IRQn);
			PWR_BackupAccessCmd(ENABLE);
			RTC_WaitForLastTask();
			RCC_RTCCLKCmd(DISABLE);
			RTC_SetCounter(Time_raw);
			RCC_RTCCLKCmd(ENABLE);
			RTC_WaitForSynchro();
			RTC_WaitForLastTask();
			PWR_BackupAccessCmd(DISABLE);
			NVIC_EnableIRQ(RTC_IRQn);
		}
	}
	

	UART_RX.RX_Ready_to_Parse = 0;
}

